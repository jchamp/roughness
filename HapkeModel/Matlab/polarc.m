function [hfig, hcol] = polarc(phi,theta,magnitude,varargin)
%POLARC Representation of data from a semi-hemisphere surface on a polar 
%coordinate plot with colors
%   POLARC(PHI, THETA, MAGNITUDE) makes a plot using polar coordinates. The
%   angle THETA is represented in range and the angle PHI is represented in
%   azimuth.
%   PHI, THETA and MAGNITUDE must be vectors of the same size. PHI and
%   THETA are in degrees and the unity of MAGNITUDE depends on the user.
%
%   [HFIG,HCOL] = POLARC(...) returns a handle to the figure in HFIG and a
%   handle to the colorbar in HCOL.
%
%   Notes 
%   =====
%   VARARGIN argument: 'property','value' pairs that could modify plot 
%       characteristics. Property names must be specified as quoted 
%       strings. Be careful, property names and their values are not case 
%       sensitive. All properties are optional and briefly described
%       below :
%
%   - 'DeltaRange'          Scalar that gives the distance, in degrees, 
%                           between two reference circles in range (THETA).
%                           If not given, the default value, 20�, is taken.
%
%   - 'DeltaRangeOffset'    Scalar that gives the offset, in degrees, i.e
%                           the value for the first circle plotted (THETA).
%                           If not given, the default value, 10�, is taken.
%
%   - 'DeltaAzimuth'        Scalar that gives the distance, in degrees, 
%                           between two reference lines in azimuth (PHI).
%                           If not given, the default value, 30�, is taken.
%
%   - 'DeltaAzimuthOffset'  Scalar that gives the offset, in degrees, i.e
%                           the value for the first line plotted (PHI).
%                           If not given, the default value, 0�, is taken.
%
%   - 'RangeValues'         Scalar or vector that give the exact values, in
%                           degrees, of the different reference cicrcles in
%                           range (THETA). This parameter replace
%                           'DeltaRange' and 'DeltaRangeOffset'.
%
%   - 'AzimuthValues'       Scalar or vector that give the exact values, in
%                           degrees, of the different reference lines in
%                           azimuth (PHI). This parameter replace
%                           'AzimuthRange' and 'AzimuthRangeOffset'.
%
%   - 'Phi_RangeNumbers'    Scalar that gives the azimuth value (PHI)
%                           corresponding to the direction where values of
%                           the diffents reference circles (THETA) are
%                           written. 
%                           If not given, the default value, 75�, is taken.
%
%   - 'CAxis'               Vector giving the range [min max] of the
%                           colorbar. If not given, the limit values are
%                           chosen automatically.
%
%   - 'ColorbarTitle'       String to add a title on the color bar and thus
%                           precise the values represented.
%
%   - 'Title'               String to add a title on the figure.
%
%   - 'Star'                Vector giving the position [PHI THETA] of a
%                           point that will be represented by a black star.
%
%   - 'StarAndColor'        Cell containing first the position vector of a
%                           point [PHI THETA] that will be represented as a
%                           star with a color defined by a second vector of
%                           3 values between 0 and 1 [R, G, B]. The
%                           argument is thus {[PHI THETA],[R G B]}.
%
%   - 'Profile'             Scalar containing the azimuthal angle PHI of
%                           the profile to represent, or cell containing
%                           first this scalar and then a colored vector of
%                           3 values between 0 and 1 [R, G, B]. The
%                           argument is thus PHI or {PHI,[R G B]}. Note
%                           that the profile will covered the data with
%                           azimuthal angle equals to PHI and PHI+180 (The
%                           angle have to be in degrees here) and the 
%                           default color is white. Another scalar can be
%                           added to represents the linewidth.
%
%   - 'Print'               String containing the name of the file to save
%                           the figure. It will be saved within the current
%                           folder as a PNG file with a 500 Dots-per-inch 
%                           resolution defined in this function.
%
%   Examples
%   ========
%   clear all; close all; clc;
%   theta_i = 35; phi_i = 160;
%   theta = 0:1:90; phi = 0:1:360;
%   [phi,theta]=meshgrid(phi,theta);
%   phase_angle = inline(['acosd( (sind(theta_i).*sind(theta)) .* ',...
%    '(cosd(phi_i-phi)) + cosd(theta_i).*cosd(theta))'],'theta_i',... 
%    'phi_i','theta','phi');
%   magnitude = phase_angle(theta_i,phi_i,theta,phi);
%   [hfig, hcol] = polarc(phi,theta,magnitude,'Title',...
%    'Test figure for the function "polarc"','Star',[phi_i,theta_i],...
%    'ColorbarTitle','Phase angle (degrees)','Print','fig_test');
%
%   See also
%   ========
%   MESHGRID, POL2CART, SURFACE, SHADING, COLORBAR, CAXIS, PLOT, PLOT3, 
%   AXIS
%
%   Versions
%   ========
%   1   (Jason Champion - 2013/03/02) Original function.
%   2   (Jason Champion - 2013/03/06) Adding possibility to plot a line
%       representing a profile, in range, with a given color, if defined.
%
%   Copyright free function originally made by Jason Champion.
%   Revision: 2
%   Date: 2013/03/06 10:25

%% CASE WITH NOT ENOUGH ARGUMENTS %%
%==================================%
if nargin < 3
    error(['At least, arguments "phi", "theta" and "magnitude"',...
        ' have to be given.'])
end

%% VERIFICATION %%
%================%
% Verification of the argument sizes
if length(unique([size(phi,1) size(theta,1) size(magnitude,1)]))~=1 || ...
        length(unique([size(phi,2) size(theta,2) size(magnitude,2)]))~=1
    error(['Arguments "phi", "theta" and "magnitude"',...
        ' must have the same size.'])
end

%% OPTIONAL ARGUMENTS AT THE START %%
%===================================%
% Default values
deltarange = 20;
deltarangeoffset = 10;
deltaazimuth = 30;
deltaazimuthoffset = 0;
rangevalues = [];
azimuthvalues = [];
phi_rangenumbers = 75;
if nargin > 3
    N = length(varargin);
    for j=1:2:N
        switch varargin{j}
            case 'DeltaRange'
                deltarange = varargin{j+1};
            case 'DeltaRangeOffset'
                deltarangeoffset = varargin{j+1};
            case 'DeltaAzimuth'
                deltaazimuth = varargin{j+1};
            case 'DeltaAzimuthOffset'
                deltaazimuthoffset = varargin{j+1};
            case 'RangeValues'
                rangevalues = varargin{j+1};
            case 'AzimuthValues'
                azimuthvalues = varargin{j+1};
            case 'Phi_RangeNumbers'
                phi_rangenumbers = varargin{j+1};
        end
    end
end

%% PLOTTING DATA %%
%=================%
hfig = figure('color','white');
[x,y] = pol2cart(phi*pi/180,theta);
surface(x,y,magnitude)

% Modifications
shading flat
set(gca,'XTick',[],'YTick',[])
axis square
hcol = colorbar;
set(hcol,'fontsize',15,'box','on');
set(gca,'fontsize',15,'box','on')
set(gca,'visible','off')
axe=((max(theta(:))-min(theta(:))))*1.2;
axe=axe*[-1 1 -1 1];
axis(axe);

% Adding reference lines in range (representing theta)
range = max(theta(:))-min(theta(:));
if ~isempty(rangevalues)
    vect = rangevalues;
else
    vect = floor(min(theta(:))/10)+deltarangeoffset:deltarange:...
        ceil(max(theta(:)));
end

for i=vect;
    t=linspace(0,360,100);
    [t,r]=meshgrid(t,[i i]);
    [xi,yi] = pol2cart(t*pi/180,r);
    surface(xi,yi,ones(size(xi))*max(magnitude(:)))
    if i>0
        text((i/range+0.06)*max(theta(:))*cosd(phi_rangenumbers),...
           (i/range+0.06)*max(theta(:))*sind(phi_rangenumbers),...
           max(magnitude(:)),[num2str(i) '°'],...
           'HorizontalAlignment', 'center', 'VerticalAlignment', ...
           'middle','fontsize',10);
    end
end

% Adding reference lines in azimuth (representing phi)
if ~isempty(azimuthvalues)
    vect = azimuthvalues;
else
    vect = deltaazimuthoffset:deltaazimuth:360;
end
for i=vect;
    line([0 cosd(i)*max(theta(:))],...
        [0 sind(i)*max(theta(:))],...
        [max(magnitude(:)) max(magnitude(:))],...
        'linestyle','-.','color',[0 0 0])
    if i<360
        text(1.1*(cosd(i)*max(theta(:))),...
            1.1*sind(i)*max(theta(:)),[num2str(i) '°'],...
            'HorizontalAlignment', 'center', 'VerticalAlignment', 'middle',...
            'fontsize',15);
    end
end

%% OPTIONAL ARGUMENTS AT THE END %%
%=================================%
if nargin > 3
    N = length(varargin);
    for j=1:2:N
        switch varargin{j}
            case 'CAxis'
                caxis(varargin{j+1})
            case 'ColorbarTitle'
                set(get(hcol,'YLabel'),'String',varargin{j+1},...
                    'fontsize',15);
            case 'Title'
                title(varargin{j+1});
            case 'Star'
                hold on
                vect = varargin{j+1};
                [xs,ys]=pol2cart(vect(1)*pi/180,vect(2));
                plot3(xs,ys,max(magnitude(:)),'-p','markersize',8,...
                    'MarkerEdgeColor',[0,0,0],'MarkerFaceColor',[0 0 0])
                hold off
            case 'StarAndColor'
                hold on
                cellarg = varargin{j+1};
                pos = cellarg{1};
                color = cellarg{2};
                [xs,ys]=pol2cart(pos(1)*pi/180,pos(2));
                plot3(xs,ys,max(magnitude(:)),'-p','markersize',8,...
                    'MarkerEdgeColor',color,'MarkerFaceColor',color)
                hold off
            case 'Profile'
                if iscell(varargin{j+1})
                    cellarg = varargin{j+1};
                    phivalue = cellarg{1};
                    color = cellarg{2};
                    if length(cellarg)==2
                        lw = 2;
                    else
                        lw = cellarg{3};
                    end
                else
                    phivalue = varargin{j+1};
                    color = [1 1 1];
                    lw = 2;
                end
                line([0 cosd(phivalue)*max(theta(:))],...
                    [0 sind(phivalue)*max(theta(:))],...
                    [max(magnitude(:)) max(magnitude(:))],...
                    'linestyle','-','color',color,'linewidth',lw)
                line([0 cosd(phivalue+180)*max(theta(:))],...
                    [0 sind(phivalue+180)*max(theta(:))],...
                    [max(magnitude(:)) max(magnitude(:))],...
                    'linestyle','-','color',color,'linewidth',lw)
            case 'Print'
                print('-dpng','-r500',[varargin{j+1} '.png'])
        end
    end
end
  
end
