function M=M(mu0_f, mu_f)

M = Pm(mu0_f).*(H(mu_f)-1) + Pm(mu_f).*(H(mu0_f)-1) + ...
		Pmm().*(H(mu0_f)-1).*(H(mu_f)-1);

end