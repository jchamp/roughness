function E1=E1(x,tb_f)

E1 = exp( -(2.0/pi)*( 1.0./(tan(tb_f)) ).*( 1.0./(tan(x)) ) );

end