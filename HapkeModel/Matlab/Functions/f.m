function f=f(phi_f)

f = exp( -2.0*tan(phi_f/2.0) );

end