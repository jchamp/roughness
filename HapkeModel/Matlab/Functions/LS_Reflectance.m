function LS_Reflectance=LS_Reflectance(mu0_f,mu_f)

global w
LS_Reflectance = (w/(4*pi)) * mu0_f./(mu0_f+mu_f);

end