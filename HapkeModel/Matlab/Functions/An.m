function An=An(n)

OddFactorial = 1;
EvenFactorial = 1;
% Case of n is an even number
if (fix(n/2)==n/2)
    An = 0;
    % Case of n is an odd number
else
    for ic = 1:2:n
        OddFactorial = OddFactorial * ic;
    end
    for ic = 2:2:n+1
        EvenFactorial = EvenFactorial * ic;
    end
    An = ( ((-1.0)^((n+1)/2)) / n ) * (OddFactorial/EvenFactorial);
end

end