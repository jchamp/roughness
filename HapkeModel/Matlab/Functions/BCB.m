function BCB=BCB(g_f)

global BC0 hC
factor = (1.0/hC)*tan(g_f/2);
if (BC0~=0)
    BCB = 1.0 + ( 1.0 + (1.0-exp(-factor))./factor ) ./ ...
        ( 2*((1+factor).^2) );
else
    BCB = 1.0;
end

end