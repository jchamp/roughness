function E2=E2(x,tb_f)

E2 = exp( -(1.0/pi)*( 1.0./tan(tb_f).^2 )*( 1.0./tan(x).^2 ) );

end