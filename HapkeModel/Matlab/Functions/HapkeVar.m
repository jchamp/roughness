clear all
close all
clc

% Definition of the global variables
global w tb BS0 BC0 hS hC i e phii phie phi mu0	mu mu0p mup g p S rS rM ...
    r coeffs ncoeffs ndim1 ndim2 %#ok<*NUSED>

% Recuperation of the variables.
load data.mat % En attendant mieux (fichier ./In/Param.asc)

% Modification of the variables.
tb = tb*pi/180;

% Matrix operations.
% dim 1 : zenithal angles.
% dim 2 : azimutal angles.
i = zeros(ndim1,ndim2);
i(:) = ideg*pi/180;
phii = phiideg*180/pi;
e = zeros(ndim1,ndim2);
for ic = 1:ndim1
	e(ic,:) = (edeg_start+(ic-1)*edeg_delta)*pi/180;
end
phie = zeros(ndim1,ndim2);
for ic = 1:ndim2
		phie(:,ic) = (phiedeg_start+(ic-1)*phiedeg_delta)*pi/180;
end
mu0 = cos(i);
mu = cos(e);
g = acos((mu.*mu0)+(sin(e).*sin(i).*cos(phie-phii)));
phi = acos(cos(phie-phii));
p = ones(size(e));
for ic = 1:ncoeffs
	p = p + coeffs(ic)*Pn(ic,cos(g));
end