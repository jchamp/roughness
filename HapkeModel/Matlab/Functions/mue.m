function mue=mue(tb_f)

global i e ndim1 ndim2 phi

E1i = E1(i,tb_f);
E1e = E1(e,tb_f);
E2i = E2(i,tb_f);
E2e = E2(e,tb_f);
mue = zeros(size(e));
for i1=1:ndim1
    for i2=1:ndim2
        i_i = i(i1,i2);
        e_i = e(i1,i2);
        phi_i = phi(i1,i2);
        E1i_i = E1i(i1,i2);
        E1e_i = E1e(i1,i2);
        E2i_i = E2i(i1,i2);
        E2e_i = E2e(i1,i2);
        if (i_i <= e_i)
            mue(i1,i2) = E2e_i - (sin(phi_i/2).^2).*E2i_i;
            mue(i1,i2) = mue(i1,i2) ./ ( 2-E1e_i-(phi_i/pi).*E1i_i );
            mue(i1,i2) = chi(tb_f) * ( cos(e_i) + sin(e_i)*tan(tb_f)*...
                mue(i1,i2) );
        else
            mue(i1,i2) = cos(phi_i)*E2i_i + (sin(phi_i/2).^2).*E2e_i;
            mue(i1,i2) = mue(i1,i2) ./ ( 2-E1i_i-(phi_i/pi).*E1e_i );
            mue(i1,i2) = chi(tb_f) * ( cos(e_i) + sin(e_i)*tan(tb_f)*...
                mue(i1,i2) );
        end
    end
end

end