function Pn=Pn(n,x)

switch n
    case 0
        Pn = 1;
    case 1
        Pn = x;
    case 2
        Pn = 0.5*(3*(x.^2)-1);
    case 3
        Pn = 0.5*(5*(x.^3)-3*x);
    case 4
        Pn = 0.125*(35*(x.^4)-30*(x.^2)+3);
    case 5
        Pn = 0.125*(63*(x.^5)-70*(x.^3)+15*x);
    otherwise
        Pn = 0;
        disp(char(10))
        warning([char(10) 'The Legendre polynomial of '...
            'degree ' num2str(n) ...
            ' is not available here.' char(10) ...
            'It won''t be taken into account for the calculation ' ...
            'of the phase function.']) %#ok<WNTAG>
end

end