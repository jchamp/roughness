function Pmm=Pmm()

global ncoeffs coeffs
Pmm = 1;
% Calculation only when 'ic' (which is n) is a odd number, otherwise An=0.
for ic = 1:2:ncoeffs
	Pmm = Pmm - ((An(ic))^2)*coeffs(ic);
end

end