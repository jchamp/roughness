function BSH=BSH(g_f)

global BS0 hS
if (BS0~=0)
    BSH = 1.0 + BS0./( 1.0+(tan(g_f/2.0)/hS) );
else
    BSH = 1.0;
end

end