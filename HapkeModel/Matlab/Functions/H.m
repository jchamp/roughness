function H=H(x)

global w
r0 = (1-sqrt(1-w))/(1+sqrt(1-w));
H = 1 - w*x.*( r0 + 0.5*(1-2*r0*x).*log((1+x)./x) );
H = 1./H;
H(x<=0.001) = (1+2*x(x<=0.001)) ./ (1+2*x(x<=0.001)*sqrt(1-w));

end