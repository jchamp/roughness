function ShadFunc=ShadFunc(tb_f)

global i e mu0 mu phi

mup = mue(tb_f);
etap = eta(e,tb_f);
eta0p = eta(i,tb_f);
rap = zeros(size(e));
rap(i<=e) = mu0(i<=e)./eta0p(i<=e);
rap(i>e) = mu(i>e)./etap(i>e);
ShadFunc = (mup./etap).*(mu0./eta0p).*chi(tb_f)./...
    ( 1-f(phi)+f(phi)*chi(tb_f).*rap );

end