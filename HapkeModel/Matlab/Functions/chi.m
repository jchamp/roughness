function chi=chi(tb_f)

chi = 1 ./ sqrt( 1+pi*(tan(tb_f).^2) );

end