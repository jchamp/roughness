HapkeVar;
global tb mu0 mu mu0p mup g p S rS rM r  %#ok<*NUSED> 
if (tb==0)
    rS = LS_Reflectance(mu0, mu) .* p .* BSH(g) .* BCB(g);
    rM = LS_Reflectance(mu0, mu) .* M(mu0, mu) .* BCB(g);
    r = rS + rM;
else
    mu0p = mu0e(tb);
    mup = mue(tb);
    rS = LS_Reflectance(mu0p, mup) .* p .* BSH(g) .* BCB(g);
    rM = LS_Reflectance(mu0p, mup) .* M(mu0p, mup) .* BCB(g);
    S = ShadFunc(tb);
    r = (rS+rM).*S;
end
SaveData;