function Pm=Pm(x)

global ncoeffs coeffs
Pm = 1;
% Calculation only when 'ic' (which is n) is a odd number, otherwise An=0.
for ic = 1:2:ncoeffs
	Pm = Pm + An(ic)*coeffs(ic)*Pn(ic,x);
end

end