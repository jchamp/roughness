function eta=eta(x,tb_f)

eta = chi(tb_f)*( cos(x)+sin(x).*tan(tb_f).*E2(x,tb_f)./( 2-E1(x,tb_f) ) );

end