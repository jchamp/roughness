clear all
close all
clc
addpath(pwd)
addpath('./Out')

system('rm -f *.asc *.png');
ModelRun;

load data.mat;

str = {'rS','rM','r'};
for j = 1:length(str)
    eval(['[~, ~] = polarc(phie*180/pi,e*180/pi,',...
        str{j} ',''Title'',''' str{j} '''',...
        ',''Star'',[phii(1)*180/pi,i(1)*180/pi],',...
        '''ColorbarTitle'',''BRDF'',''Print'',''./Out/',...
        str{j} ''');'])
	close all
    if j<3 && ~isempty(find(S>0, 1, 'first'))
        eval(['[~, ~] = polarc(phie*180/pi,e*180/pi,',...
            str{j} '.*S,''Title'',''' str{j} '*S''',...
            ',''Star'',[phii(1)*180/pi,i(1)*180/pi],',...
            '''ColorbarTitle'',''BRDF'',''Print'',''./Out/S_',...
            str{j} ');'])
	close all
    end
end

if ~isempty(find(S>0, 1, 'first'))
	[~, ~] = polarc(phie*180/pi,e*180/pi,S,'Title','S',...
        'Star',[phii(1)*180/pi,i(1)*180/pi],...
        'CAxis',[0 1],'Print','./Out/S'); %#ok<IJCL>
end

%exit