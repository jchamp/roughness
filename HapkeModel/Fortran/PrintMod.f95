MODULE PrintMod
!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
!
! ==============================================================================
!                   	    	Module PrintMod
!             First creacted by Jason Champion on the April 12 2013   
! ==============================================================================
!
! Introduction
! =============
! This module was created to easily print data in the terminal or write them
! in a file. It includes 5 functions or subroutines (described one by one 
! after) :
! - PrintMat(mat,nl,nc)
! - PrintMatExt(mat,nl,nc,lmin,lmax,cmin,cmax)
! - PrintMat_Text(mat,nl,nc,text)
! - PrintMatExt_Text(mat,nl,nc,lmin,lmax,cmin,cmax,text)
! - WriteOut(mat,nl,nc,namefile)
! 
! Compilation
! ============
! gfortran -c PrintMod.f95
!
!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!

CONTAINS

!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
! Subroutine PrintMat(mat,nl,nc)
!
!  Print the matrix 'mat' (nl*nc) in the terminal after a line feed.
!
!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
SUBROUTINE PrintMat(mat,nl,nc)
	implicit none
	integer :: il
	integer, intent(in) :: nl, nc
	real, dimension(nl,nc), intent(in) :: mat
	print *,
	do il=1,nl,1
		print *,mat(il,:)
	end do
END SUBROUTINE

!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
! Subroutine PrintMatExt(mat,nl,nc,lmin,lmax,cmin,cmax)
!
! Print a part of the matrix 'mat' (nl*nc), in the terminal, corresponding to
!	the limit index (lmin:lmax,cmin:cmax). A line feed is also done before.
!
!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
SUBROUTINE PrintMatExt(mat,nl,nc,lmin,lmax,cmin,cmax)
	implicit none
	integer :: il
	integer, intent(in) :: nl, nc, lmin, lmax, cmin, cmax
	real, dimension(nl,nc), intent(in) :: mat
	print *,
	do il=lmin,lmax,1
		print *,mat(il,cmin:cmax)
	end do
END SUBROUTINE

!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
! Subroutine PrintMat_Text(mat,nl,nc,text)
!
!  Print the matrix 'mat' (nl*nc) in the terminal after a line feed and a
!	sentence given by the user in the variable 'text'.
!
!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
SUBROUTINE PrintMat_Text(mat,nl,nc,text)
	implicit none
	integer :: il
	integer, intent(in) :: nl, nc
	real, dimension(nl,nc), intent(in) :: mat
	character (len=*),intent(in) :: text
	print *,
	print *, text
	do il=1,nl,1
		print *,mat(il,:)
	end do
END SUBROUTINE

!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
! Subroutine PrintMatExt_Text(mat,nl,nc,lmin,lmax,cmin,cmax,text)
!
!  Print a part of the matrix 'mat' (nl*nc), in the terminal, corresponding to
!	the limit index (lmin:lmax,cmin:cmax). A line feed is also done before with
!	the prinf of a sentence given by the user in the variable 'text'.
!
!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
SUBROUTINE PrintMatExt_Text(mat,nl,nc,lmin,lmax,cmin,cmax,text)
	implicit none
	integer :: il
	integer, intent(in) :: nl, nc, lmin, lmax, cmin, cmax
	real, dimension(nl,nc), intent(in) :: mat
	character (len=*),intent(in) :: text
	print *,
	print *, text
	do il=lmin,lmax,1
		print *,mat(il,cmin:cmax)
	end do
END SUBROUTINE


!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
! Subroutine WriteOut(mat,nl,nc,namefile)
!
!  Save of the matrix 'mat' (nl*nc) in a file whose path, name and  extension
!	are given in the argument 'namefile'. The writing replace the previous file
! 	if it exist and is done in a formatted sequential way.
!
!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
SUBROUTINE WriteOut(mat,nl,nc,namefile)
	implicit none
	integer :: il, ios, length
	integer, intent(in) :: nl, nc
	real, dimension(nl,nc), intent(in) :: mat
	character (len=*),intent(in) :: namefile
	character (len=40) :: myfmt, str
	length=1
	if (nc>9) then
		length=length+1
		if (nc>99) then
			length=length+1
			if (nc>999) then
				length=length+1
			end if
		end if
	end if
	write(str,'(" (""("",I", I3 ,",""F15.10)"") ")') length
	write(myfmt,str) nc
	open(unit = 12, & 
		file = namefile, &
		form = "formatted", &
		access = "sequential", &
		action = "write", &
		status = "replace", &
		position = "rewind", &
		iostat = ios)
	if ( ios /= 0 ) then
		print *,' '
		print *,"Error : File is not open."
		print *,' '
		STOP
	else
		do il=1,nl,1
			write(unit=12,fmt=myfmt) mat(il,:)
		end do
		close(unit=12)
	endif
END SUBROUTINE

END MODULE
