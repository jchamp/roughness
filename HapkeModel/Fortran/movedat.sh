#!/bin/bash

echo -e "\nSauvegarde dans ../Out/Save de : "
cd ./In/
for var in `ls *.asc`
do 
	long="${#var}"	
	varnew=${var:0:$long-4}"_"$1".asc"
	cp $var ../Out/Save/$varnew
	echo -e "  - "$varnew
done
cd ..

cd ./Out/
for var in `ls *.asc`
do 
	long="${#var}"	
	varnew=${var:0:$long-4}"_"$1".asc"
	cp $var ./Save/$varnew
	echo -e "  - "$varnew
done
for var in `ls *.png`
do 
	long="${#var}"	
	varnew=${var:0:$long-4}"_"$1".png"
	cp $var ./Save/$varnew
	echo -e "  - "$varnew
done
