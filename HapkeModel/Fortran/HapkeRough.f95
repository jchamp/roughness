MODULE HapkeRough
!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
!
! ==============================================================================
!                   	    	Module HapkeRough             	    	
!             First creacted by Jason Champion on the April 14 2013           
! ==============================================================================
!
! Introduction
! =============
! This module contains functions in order to improved the Hapke model by taking
! into account the macroscopic roughness (cf. Hapke, 1984).
! It includes 8 functions or subroutines (described one by one after) :
! - chi(tb_f)
! - f(phi_f)
! - E1(x,tb_f)
! - E2(x,tb_f)
! - eta(x,tb_f)
! - mu0e(tb_f)
! - mue(tb_f)
! - ShadFunc(tb_f)
!
! See also
! =========
! Module HapkeVar
! Module HapkeModel
! Module PrintMod
! 
! Compilation
! ============
! gfortran -c HapkeRough.f95
!
!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!

USE HapkeVar

CONTAINS

!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
! Function chi(tb_f)
!
!  Calculation of the function chi appearing in many other functions used to 
!	correct the Hapke model for a rough surface. The argument 'tb_f' is the mean
!	slope angle in radians.
!
!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
FUNCTION chi(tb_f)
	implicit none
	real, intent(in) :: tb_f
	real :: chi
	chi = 1 / sqrt( 1+pi*(tan(tb_f)**2) )
END FUNCTION

!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
! Function f(phi_f)
!
!  Calculation of the function f representing the fraction of the non-visible 
!	area of the surface which is hidden in the illumination shadowing. The only
!	argument phi_f is the relative azimuthal angle between the source of light 
!	and the detector.
!
!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
FUNCTION f(phi_f)
	implicit none
	real, dimension(ndim1, ndim2), intent(in) :: phi_f
	real, dimension(ndim1, ndim2) :: f
	f = exp( -2.0*tan(phi_f/2.0) )
	where(f>1.0)
		f=0.0
	end where
	! NOTE :
	! Because of the constant 'pi' precision, it appears that the function f is
	! sometimes greater than one when the relative azimutal angle tend to pi/2.
	! It is not possible and that's because the tan(pi/2.0) is evaluated as if 
	! it was tan((pi+dphi_f)/2.0), where dphi_f is an infinitely small value.
	! The result of the tangent is then -Inf rather than +Inf leading to an
	! unphysical value in the calculation of f. The effect is correct by 
	! replacing values superior to 1.0 by 0.0 (the real value for an angle of 
	! pi/2).
END FUNCTION

!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
! Function E1(x,tb_f)
!
!  Calculation of the function 'E1' appearing in the zenithal effective cosinus 
!	expressions. This function depends on one of the real zenithal angles (i or
!	e) as the variable 'x' and on the mean slope angle 'tb_f'.
!
!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
FUNCTION E1(x,tb_f)
	implicit none
	real, dimension(ndim1, ndim2), intent(in) :: x
	real, intent(in) :: tb_f
	real, dimension(ndim1, ndim2) :: E1
	E1 = exp( -(2.0/pi)*( 1.0/(tan(tb_f)) )*( 1.0/(tan(x)) ) )
	where(E1>1.0)
		E1=0.0
	end where
	! Correction due to the precision of the constant 'pi', as the f function.
END FUNCTION

!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
! Function E2(x,tb_f)
!
!  Calculation of the function 'E2' appearing in the zenithal effective cosinus 
!	expressions. This function depends on one of the real zenithal angles (i or
!	e) as the variable 'x' and on the mean slope angle 'tb_f'.
!
!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
FUNCTION E2(x,tb_f)
	implicit none
	real, dimension(ndim1, ndim2), intent(in) :: x
	real, intent(in) :: tb_f
	real, dimension(ndim1, ndim2) :: E2
	E2 = exp( -(1.0/pi)*( 1.0/tan(tb_f)**2 )*( 1.0/tan(x)**2 ) )
	where(E2>1.0)
		E2=0
	end where
	! Correction due to the precision of the constant 'pi', as the f function.
END FUNCTION

!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
! Function eta(x,tb_f)
!
!  Calculation of the function eta (eta0e or etae) corresponding to the
!	calculation of the zenithal effective cosinus when the relative azimutal
!	angle is nought. This function depends on the real zenithal angles (i in the
!	case if eta0e and e in the case of etae) as the variable 'x' and on the mean
!	slope angle 'tb_f'.
!
!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
FUNCTION eta(x,tb_f)
	implicit none
	real, dimension(ndim1, ndim2), intent(in) :: x
	real, intent(in) :: tb_f
	real, dimension(ndim1, ndim2) :: eta
	eta = chi(tb_f)*( cos(x)+sin(x)*tan(tb_f)*E2(x,tb_f)/( 2-E1(x,tb_f) ) )
END FUNCTION

!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
! Function mu0e(tb_f)
!
!  Calculation of the function mu0e corresponding to the incident zenithal
!	effective cosinus. This function implicitly depends on the real zenithal
!	angles and the relative azimutal angle between the source of light and the
!	detector, but also on the mean slope angle 'tb_f' which is the only argument 
!	of this function.
!
!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
FUNCTION mu0e(tb_f)
	implicit none
	real, intent(in) :: tb_f
	real, dimension(ndim1, ndim2) :: mu0e, E1i, E1e, E2i, E2e
	real :: i_i, e_i, phi_i, E1i_i, E1e_i, E2i_i, E2e_i
	integer :: i1, i2
	E1i = E1(i,tb_f)
	E1e = E1(e,tb_f)
	E2i = E2(i,tb_f)
	E2e = E2(e,tb_f)
	do i1=1,ndim1
		do i2=1,ndim2
			i_i = i(i1,i2)
			e_i = e(i1,i2)
			phi_i = phi(i1,i2)
			E1i_i = E1i(i1,i2)
			E1e_i = E1e(i1,i2)
			E2i_i = E2i(i1,i2)
			E2e_i = E2e(i1,i2)
			if (i_i .le. e_i) then
				mu0e(i1,i2) = cos(phi_i)*E2e_i + (sin(phi_i/2)**2)*E2i_i
				mu0e(i1,i2) = mu0e(i1,i2) / ( 2-E1e_i-(phi_i/pi)*E1i_i )
				mu0e(i1,i2) = chi(tb_f) * ( cos(i_i) + sin(i_i)*tan(tb_f)*mu0e(i1,i2) ) 
			else
				mu0e(i1,i2) = E2i_i - (sin(phi_i/2)**2)*E2e_i
				mu0e(i1,i2) = mu0e(i1,i2) / ( 2-E1i_i-(phi_i/pi)*E1e_i )
				mu0e(i1,i2) = chi(tb_f) * ( cos(i_i) + sin(i_i)*tan(tb_f)*mu0e(i1,i2) ) 
			end if
		end do
	end do
END FUNCTION

!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
! Function 'mue(tb_f)'
!
!  Calculation of the function mue corresponding to the emergent zenithal
!	effective cosinus. This function implicitly depends on the real zenithal
!	angles and the relative azimutal angle between the source of light and the
!	detector, but also on the mean slope angle 'tb_f' which is the only argument 
!	of this function.
!
!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
FUNCTION mue(tb_f)
	implicit none
	real, intent(in) :: tb_f
	real, dimension(ndim1, ndim2) :: mue, E1i, E1e, E2i, E2e
	real :: i_i, e_i, phi_i, E1i_i, E1e_i, E2i_i, E2e_i
	integer :: i1, i2
	E1i = E1(i,tb_f)
	E1e = E1(e,tb_f)
	E2i = E2(i,tb_f)
	E2e = E2(e,tb_f)
	do i1=1,ndim1
		do i2=1,ndim2
			i_i = i(i1,i2)
			e_i = e(i1,i2)
			phi_i = phi(i1,i2)
			E1i_i = E1i(i1,i2)
			E1e_i = E1e(i1,i2)
			E2i_i = E2i(i1,i2)
			E2e_i = E2e(i1,i2)
			if (i_i .le. e_i) then
				mue(i1,i2) = E2e_i - (sin(phi_i/2)**2)*E2i_i
				mue(i1,i2) = mue(i1,i2) / ( 2-E1e_i-(phi_i/pi)*E1i_i )
				mue(i1,i2) = chi(tb_f) * ( cos(e_i) + sin(e_i)*tan(tb_f)*mue(i1,i2) )
			else
				mue(i1,i2) = cos(phi_i)*E2i_i + (sin(phi_i/2)**2)*E2e_i
				mue(i1,i2) = mue(i1,i2) / ( 2-E1i_i-(phi_i/pi)*E1e_i )
				mue(i1,i2) = chi(tb_f) * ( cos(e_i) + sin(e_i)*tan(tb_f)*mue(i1,i2) )
			end if
		end do
	end do
END FUNCTION

!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
! Function ShadFunc(tb_f)
!
!  Cakculation of the shadowing function corresponding to the fraction of the
!	area visible from the detector which is illiminated by the source. This 
!	function implicitly depends on the real and effective zenithal angles and 
!	the relative azimutal angle between the source of light and the detector, 
!	but also on the mean slope angle 'tb_f' which is the only argument of this 
!	function.
!
!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
FUNCTION ShadFunc(tb_f)
	implicit none
	real, intent(in) :: tb_f
	real, dimension(ndim1, ndim2) :: eta0p, etap, rap, ShadFunc
	mup = mue(tb_f)
	etap = eta(e,tb_f)
	eta0p = eta(i,tb_f)
	where(i .le. e)
		rap = mu0/eta0p
	elsewhere
		rap = mu/etap
	end where
	ShadFunc = (mup/etap)*(mu0/eta0p)*chi(tb_f)/( 1-f(phi)+f(phi)*chi(tb_f)*rap )
END FUNCTION

END MODULE
