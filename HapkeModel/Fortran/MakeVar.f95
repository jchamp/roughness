PROGRAM MakeVar
!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
!
! ==============================================================================
!                   	  		Program MakeVar
!             First creacted by Jason Champion on the April 12 2013   
! ==============================================================================
!
! Introduction
! =============
! This program write the Hapke model parameters in the file './In/Param.asc' in
! a formatted way in order to use them in the programs running the model.
! The format of the created, or modified file, is the next one :
! ______________________________________________________________
! 
! SURFACE PROPERTIES
! ==================
! Single-scattering albedo w : x.xx
! Phase function :
!    - Number of coefficients : xx
!    - Coeff. number 1 : xx.xx
!    - Coeff. number 2 : xx.xx
!    - ...
! Mean slope angle (degrees) : xx.xx
!
! SHOE :
!    - Amplitude BS0 : x.xx
!    - Angular width hS : x.xxxx
! CBOE :
!    - Amplitude BC0 : x.xx
!    - Angular width hC : x.xxxx
!
! INCIDENT DIRECTION (IN DEGREES)
! ===============================
! Zenithal angle i : xx.xx
! Azimutal angle phii : xxx.xx
!
! EMERGENT DIRECTIONS (IN DEGREES)
! ================================
! Number of zenithal angles e : xxx
! Number of azimutal angles phie : xxx
! 
! Lower limit of the zenithal angle range : xx.xx
! Higher limit of the zenithal angle range : xx.xx
! Step value within the zenithal angle range : xx.xx
!  
! Lower limit of the azimutal angle range : xxx.xx
! Higher limit of the azimutal angle range : xxx.xx
! Step value within the azimutal angle range : xxx.xx
!
! ______________________________________________________________
!
! See the file './In/Param_example.asc' to have an example of such a good file.
! Note that the real file must be named 'Param.asc' and have the same format.
! You can also notice that there is sometimes spaces before values. This is 
! explained by the fact that the number have to be on the right of its reserved 
! location. For example let's take the azimutal angle of the incident direction.
! It could from 0 to 360 degrees with 2 digits in decimal. The format is thus
! xxx.xx. Here is some examples of good placement for the corresponding value :
! 360.00
! 152.50
!  95.00
!  12.05
!   1.00
!   0.00
!    .00
! 
! See also
! =========
! Module HapkeVar
! Module HapkeModel
! Module HapkeRough
! Module PrintMod
!
! Compilation
! ============
! gfortran MakeVar.f95 -o MakeVar.exe
!
!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!

IMPLICIT NONE
real :: w, tb, ideg, phiideg, edeg_start, edeg_stop, edeg_delta, &
	phiedeg_start, phiedeg_stop, phiedeg_delta, BS0, BC0, hS, hC
integer :: ncoeffs, ndim1, ndim2, ic, ios
real, dimension(:), allocatable :: coeffs
 character(len=2) :: istring

print *,' '
print *,' '
print *,"Opening './In/Param.asc' file..."

open(unit = 11, & 
	file = "./In/Param.asc", &
	form = "formatted", &
	access = "sequential", &
	action = "write", &
	position = "rewind", &
	iostat = ios)
if ( ios /= 0 ) then
	print *,' '
	print *,"Error : File is not open."
	print *,' '
	STOP
else
	! SURFACE PROPERTIES
	print *,' '
	print *,"Let's start with the surface properties."
	! Single-scattring albedo
	print *,"Single-scattering albedo w : "
	read *, w
	write(unit=11,fmt='(A18)') "SURFACE PROPERTIES"
	write(unit=11,fmt='(A18)') "=================="
	write(unit=11,fmt='(A29,F4.2)') "Single-scattering albedo w : ", w
	! Phase function
	print *,"Degree of Legendre Polynomials development for the phase ", &
		"function : "
	read *, ncoeffs
	write(unit=11,fmt='(A16)') "Phase function :"
	write(unit=11,fmt='(A30,I2)') "   - Number of coefficients : ", ncoeffs
	if(.not. allocated(coeffs)) then
		allocate(coeffs(ncoeffs))
	end if
	do ic=1,ncoeffs
		write(istring,'(I2)') ic
		print *,"     Coefficient number ",istring," : "
		read *, coeffs(ic)
		write(unit=11,fmt='(A18,I2,A3,F5.2)') "   - Coeff. number", ic, &
			" : ", coeffs(ic)
		! note : F5.2 rather than F4.2 because the coefficient may be negative.
	end do
	deallocate(coeffs)
	! Mean slope angle
	print *,"Mean slope angle (in degrees) : "
	read *, tb
	write(unit=11,fmt='(A29,F5.2)') "Mean slope angle (degrees) : ", tb
	! SHOE
	write(unit=11,fmt='(A1)') ' '
	write(unit=11,fmt='(A6)') "SHOE :"
	print *,"Amplitude of the SHOE : "
	read *, BS0
	write(unit=11,fmt='(A21,F4.2)') "   - Amplitude BS0 : ", BS0
	print *,"Angular width of the SHOE : "
	read *, hS
	write(unit=11,fmt='(A24,F6.4)') "   - Angular width hS : ", hS
	! CBOE
	write(unit=11,fmt='(A6)') "CBOE :"
	print *,"Amplitude of the CBOE : "
	read *, BC0
	write(unit=11,fmt='(A21,F4.2)') "   - Amplitude BC0 : ", BC0
	print *,"Angular width of the CBOE : "
	read *, hC
	write(unit=11,fmt='(A24,F6.4)') "   - Angular width hC : ", hC
	
	! INCIDENT DIRECTION
	print *, ' '
	print *,"Let's look at the incident direction."
	print *,"Zenithal angle i (in degrees) : "
	read *, ideg
	write(unit=11,fmt='(A1)') ''
	write(unit=11,fmt='(A31)') "INCIDENT DIRECTION (IN DEGREES)"
	write(unit=11,fmt='(A31)') "==============================="
	write(unit=11,fmt='(A19,F5.2)') "Zenithal angle i : ", ideg
	print *,"Azimutal angle phii (in degrees) : "
	read *, phiideg
	write(unit=11,fmt='(A22,F6.2)') "Azimutal angle phii : ", phiideg

	! EMERGENT DIRECTIONS
	print *,' '
	write(unit=11,fmt='(A1)') ''
	write(unit=11,fmt='(A32)') "EMERGENT DIRECTIONS (IN DEGREES)"
	write(unit=11,fmt='(A32)') "================================"
	print *,"We are now going to define the range and step for the ", &
		"emergent directions."
	print *,"Minimal value of the zenithal angle e (in degrees) : "
	read *, edeg_start
	print *,"Maximal value of the zenithal angle e (in degrees) : "
	read *, edeg_stop
	print *,"Step value of the zenithal angle e (in degrees) : "
	read *, edeg_delta
	print *,"Minimal value of the azimutal angle phie (in degrees) : "
	read *, phiedeg_start
	print *,"Maximal value of the azimutal angle phie (in degrees) : "
	read *, phiedeg_stop
	print *,"Step value of the azimutal angle phie (in degrees) : "
	read *, phiedeg_delta	
	ndim1 = nint((edeg_stop-edeg_start)/edeg_delta)+1
	ndim2 = nint((phiedeg_stop-phiedeg_start)/phiedeg_delta)+1
	write(unit=11,fmt='(A30,I3)') "Number of zenithal angles e : ", ndim1
	write(unit=11,fmt='(A33,I3)') "Number of azimutal angles phie : ", ndim2
	write(unit=11,fmt='(A1)') ''
	write(unit=11,fmt='(A42,F5.2)') &
		"Lower limit of the zenithal angle range : ", edeg_start
	write(unit=11,fmt='(A43,F5.2)') &
		"Higher limit of the zenithal angle range : ", edeg_stop
	write(unit=11,fmt='(A45,F5.2)') &
		"Step value within the zenithal angle range : ", edeg_delta
	write(unit=11,fmt='(A1)') ''
	write(unit=11,fmt='(A42,F6.2)') &
		"Lower limit of the azimutal angle range : ", phiedeg_start
	write(unit=11,fmt='(A43,F6.2)') &
		"Higher limit of the azimutal angle range : ", phiedeg_stop
	write(unit=11,fmt='(A45,F6.2)') &
		"Step value within the azimutal angle range : ", phiedeg_delta
	close(unit=11)
end if
END PROGRAM
