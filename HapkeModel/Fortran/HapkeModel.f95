MODULE HapkeModel
!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
!
! ==============================================================================
!                   	    	Module HapkeModel                  	    	
!             First creacted by Jason Champion on the April 12 2013           
! ==============================================================================
!
! Introduction
! =============
! This module contains functions in order to use the Hapke model. It includes 8
! functions or subroutines (described one by one after) :
! - LS_Reflectance(mu0_f, mu_f)
! - H(x)
! - An(n)
! - Pm(x)
! - Pmm()
! - M(mu0_f, mu_f)
! - BSH(g_f)
! - BCB(g_f)
! - SaveData
! - BRDF
!
! See also
! =========
! Module HapkeVar
! Module HapkeRough
! Module PrintMod
!
! Compilation
! ============
! gfortran -c HapkeModel.f95
!
!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!

USE HapkeVar
USE HapkeRough
USE PrintMod

CONTAINS

!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
! Function LS_Reflectance(mu0_f, mu_f)
!
!  Calculation of the Lommel-Seeliger law appearing in the Hapke model.
!	Arguments mu0_f and mu_f are respectively the cosinus (or effective cosinus)
!	of the incident and the emergent zenithal angles.
!
!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
FUNCTION LS_Reflectance(mu0_f, mu_f)
	implicit none
	real, dimension(ndim1, ndim2), intent(in) :: mu0_f, mu_f
	real, dimension(ndim1, ndim2) :: LS_Reflectance
	LS_Reflectance = (w/(4*pi)) * mu0_f/(mu0_f+mu_f)
END FUNCTION

!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
! Function H(x)
!
!  Calculation of the H function appearing in the multiple-scattering 
!	expression. The simplified analytical formulation of Hapke (2002), showing
!	a maximum error of 1% with the numerical exact solution of Chandrasekhar 
!	(1960), is used here. The argument x here represent the cosinus, or the 
!	effective one, of a zenithal angle (e.g. i, e, ie, ee).
!
!	It is important to note that this formulation is note defined when the 
!	cosinus is equal to 0. In this case, or e.g. when x<0.001 as here, the H
!	function is then evaluated from the other Hapke (1981) formulation, 
!	showing a maximum error of 4% but with an insignificant one when x tend to
!	nought.
!
!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
FUNCTION H(x)
	implicit none
	real :: r0
	real, dimension(ndim1, ndim2), intent(in) :: x
	real, dimension(ndim1, ndim2) :: H
	r0 = (1-sqrt(1-w))/(1+sqrt(1-w))
	H = 1 - w*x*( r0 + 0.5*(1-2*r0*x)*log((1+x)/x) )
	H = 1/H
	where (x<=0.001)
		H = (1+2*x) / (1+2*x*sqrt(1-w))
	end where
END FUNCTION

!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
! Function An(n)
!
!  Calculation of the An coefficients appearing in the improved expression of 
!	the multiple-scattering formulation within the Hapke model (2002). The 
!	argument n is the degree of the corresponding Legendre polynomial.
!
!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
FUNCTION An(n)
	implicit none
	integer, intent(in) :: n
	real :: OddFactorial, EvenFactorial, An
	integer :: ic
	OddFactorial = 1
	EvenFactorial = 1
	! Case of n is an even number
	if (modulo(n,2) .eq. 0) then
		An = 0
	! Case of n is an odd number
	else
		do ic=1,n,2
			OddFactorial = OddFactorial * ic
		end do
		do ic=2,n+1,2
			EvenFactorial = EvenFactorial * ic
		end do
		An = ( ((-1.0)**((n+1)/2)) / n ) * (OddFactorial/EvenFactorial)
	end if
END FUNCTION

!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
! Function Pm(x)
!
!  Calculation of P functions appearing in the improved expression of the 
!	multiple-scattering formulation within the Hapke model (2002). The argument 
!	x here represent the cosinus, or the effective one, of a zenithal angle 
!	(e.g. i, e, ie, ee).
!
!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
FUNCTION Pm(x)
	implicit none
	real, dimension(ndim1, ndim2), intent(in) :: x
	real, dimension(ndim1, ndim2) :: Pm
	integer :: ic
	Pm = 1
	! Calculation only when 'ic' (which is n) is a odd number, otherwise An=0.
	do ic=1,ncoeffs,2
		Pm = Pm + An(ic)*coeffs(ic)*Pn(ic,x)
	end do
END FUNCTION

!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
! Function Pmm()
!
!  Calculation of the scalar P ('P' written with an unusual font) appearing in
!	the improved expression of the multiple-scattering formulation within the 
!	Hapke model (2002). Its value depending on odd An coefficients and
!	corresponding coefficients in the developpement of the phase function in
!	Legendre polynomials.
!
!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
FUNCTION Pmm()
	implicit none
	real :: Pmm
	integer :: ic
	Pmm = 1
	! Calculation only when 'ic' (which is n) is a odd number, otherwise An=0.
	do ic=1,ncoeffs,2
		Pmm = Pmm - ((An(ic))**2)*coeffs(ic)
	end do
END FUNCTION

!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
! Function M(mu0_f, mu_f)
!
!  Calculation of the M function expression which correspond to the improved
!	multiple-scattering formulation within the Hapke model (2002).
!
!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
FUNCTION M(mu0_f, mu_f)
	implicit none
	real, dimension(ndim1, ndim2), intent(in) :: mu0_f, mu_f
	real, dimension(ndim1, ndim2) :: M
	M = Pm(mu0_f)*(H(mu_f)-1) + Pm(mu_f)*(H(mu0_f)-1) + &
		Pmm()*(H(mu0_f)-1)*(H(mu_f)-1)
END FUNCTION

!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
! Function BSH(g_f)
!
!  Calculation of the backscattering function related to the shadow-hiding 
!	opposition effect (SHOE). The formulation from Hapke (1986) is used.
!
!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
FUNCTION BSH(g_f)
	implicit none
	real, dimension(ndim1, ndim2), intent(in) :: g_f
	real, dimension(ndim1, ndim2) :: BSH
	if (BS0/=0) then
		BSH = 1.0 + BS0/( 1.0+(tan(g_f/2.0)/hS) )
	else
		BSH = 1.0
	end if
END FUNCTION

!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
! Function BCB(g_f)
!
!  Calculation of the backscattering function related to the coherent 
!	backscattering opposition effect. This phenomena has been added after the
!	SHOE one in the Hapke model (2002).
!
!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
FUNCTION BCB(g_f)
	implicit none
	real, dimension(ndim1, ndim2), intent(in) :: g_f
	real, dimension(ndim1, ndim2) :: BCB, factor
	factor = (1.0/hC)*tan(g_f/2)
	if (BC0/=0) then
		BCB = 1.0 + BC0 * ( 1.0 + (1.0-exp(-factor))/factor ) / &
			( 2*((1+factor)**2) )
	else
		BCB = 1.0
	end if
END FUNCTION

!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
! Subroutine SaveData
!
!  Save of different input or output of the model in ASCI files in the folder
!	'./Out/' (These ones are able to be read in Matlab). Matrix i, phii, e, 
!	phie, rS, rM, S and r are saved in different files with a name equals to the
!	one of the variable.
!	A subroutine of the module 'PrintMod' is called to write the files.
!
!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
SUBROUTINE SaveData
	implicit none
	call WriteOut(i,ndim1,ndim2,"./Out/i.asc") 
	call WriteOut(phii,ndim1,ndim2,"./Out/phii.asc")
	call WriteOut(e,ndim1,ndim2,"./Out/e.asc")
	call WriteOut(phie,ndim1,ndim2,"./Out/phie.asc")
	call WriteOut(rS,ndim1,ndim2,"./Out/rS.asc")
	call WriteOut(rM,ndim1,ndim2,"./Out/rM.asc")
	call WriteOut(S,ndim1,ndim2,"./Out/S.asc")
	call WriteOut(r,ndim1,ndim2,"./Out/r.asc")
END SUBROUTINE

!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
! Subroutine BRDF
!
!  Calculation of the entire Hapke model with parameters defined in the file
!	'./In/Param.asc'. The outcomes and some inputs are saved in ASCI files in 
!	the folder './Out/' thank to the subroutine 'SaveData' in the same module.
!	Subroutines of modules 'HapkeVar' and 'HapkeRough' are called to collect the
!	parameters and do some necessary calculations.
!
!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
SUBROUTINE BRDF
	implicit none
	call VarRecup ! cf. HapkeVar
	if (tb==0) then
		rS = LS_Reflectance(mu0, mu) * p * BSH(g) * BCB(g)
		rM = LS_Reflectance(mu0, mu) * M(mu0, mu) * BCB(g)
		r = rS + rM
	else
		mu0p = mu0e(tb) ! cf. HapkeRough
		mup = mue(tb) ! cf. HapkeRough
		rS = LS_Reflectance(mu0p, mup) * p * BSH(g) * BCB(g)
		rM = LS_Reflectance(mu0p, mup) * M(mu0p, mup) * BCB(g)
		S = ShadFunc(tb) ! cf. HapkeRough
		r = (rS+rM)*S
	end if
	call SaveData
	call Deallocation ! cf. HapkeVar
END SUBROUTINE

END MODULE

