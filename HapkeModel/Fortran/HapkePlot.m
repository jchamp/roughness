clear all
close all
clc
addpath(pwd)

cd ./Out/
system('rm -f *.asc *.png');
cd ..
system('./ModelRun.exe');

cd ./Out/
ext = '*.asc';
chemin = fullfile('',ext);
list = dir(chemin);
n = numel(list);
for j = 1:n
    str = list(j).name;
    str = str(1:length(str)-4);
    eval([str '= load(list(' num2str(j) ').name);'])
end
clear('ext','chemin','list','n','j','str');

str = {'rS','rM','r'};
strtitle = {'Single-scattering reflectance',...
    'Multiple-scattering reflectance',...
    'Total reflectance'};
for j = 1:length(str)
    eval(['vmin = fix(1000*min(' str{j} '(:)))/1000;'])
    eval(['vmax = ceil(1000*max(' str{j} '(:)))/1000;'])
    eval(['[~, ~] = polarc(phie*180/pi,e*180/pi,',...
        str{j} ',''Title'',''' strtitle{j} '''',...
        ',''Star'',[phii(1)*180/pi,i(1)*180/pi],',...
        '''ColorbarTitle'',''Reflectance'',''CAxis'',',...
        '[vmin vmax],',...
        '''Print'',''',...
        str{j} ''');'])
	close all
    if j<3 && ~isempty(find(S>0, 1, 'first'))
        eval(['vmin = fix(1000*min(' str{j} '(:).*S(:)))/1000;'])
        eval(['vmax = ceil(1000*max(' str{j} '(:).*S(:)))/1000;'])
        eval(['[~, ~] = polarc(phie*180/pi,e*180/pi,',...
            str{j} '.*S,''Title'',''' strtitle{j} '''',...
            ',''Star'',[phii(1)*180/pi,i(1)*180/pi],',...
            '''ColorbarTitle'',''Reflectance'',''CAxis'',',...
            '[vmin vmax],',...
            '''Print'',''S_',...
            str{j} ''');'])
	close all
    end
end

if ~isempty(find(S>0, 1, 'first'))
    vmax = 1;
    if ~isempty(find(S>1, 1, 'first'))
        vmax = ceil(100*max(S(:)))/100;
    end
	[~, ~] = polarc(phie*180/pi,e*180/pi,S,'Title','S',...
        'RangeValues',30:30:90,'AzimuthValues',0:30:360,...
        'Star',[phii(1)*180/pi,i(1)*180/pi],...
        'ColorbarTitle','Fraction of visible area which is illuminated',...
        'CAxis',[0 vmax]); %#ok<IJCL>
    colormap(bone(500))
    print('-dpng','-r500','S.png')
end

cd ..

%exit
