MODULE HapkeVar
!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
!
! ==============================================================================
!                   	    	  Module HapkeVar
!             First creacted by Jason Champion on the April 12 2013           
! ==============================================================================
!  
! Introduction
! =============
! This module defines the global variables used in the calculation of the BRDF
! (bidirectional reflectance distribution function) from the Hapke model.
! It includes 8 functions or subroutines (described one by one after) :
! - VarRecup (including 'Allocation' and 'AnglesCalculation')
! - Allocation
! - Deallocation
! - Pn(n,x)
! - AnglesCalculation (including 'Pn(n,x)')
!
! Informations about the variables
! =================================
! Variables related to the surface properties :
! - w : single-scattering albedo.
! - ncoeffs : Order of the developpement in Legendre Polynomials of the phase 
!	function.
! - coeffs : Coefficients of the ncoeffs degrees of the phase function.
! - p : Values of the phase function.
! - tb : Mean slope angle (related to the roughness of the surface).
! - BSO : SHOE amplitude.
! - hS : SHOE angular width.
! - BCO : CBOE amplitude.
! - hC : CBOE angular width.
!
! Variables related to the scene geometry :
! - i : Incidente zenithal angles (source).
! - phii : Incidente azimutal angles.
! - e : Emergent zenithal angles (detector).
! - phie : Emergent azimutal angles.
! - mu0 : Cosinus of i.
! - mu : Cosinus of e.
! - mu0p : Effective mu0.
! - mup : Effective mu.
! - phi : relative azimutal angles.
! - g : Phase angles between directions (i,phii) and (e,phie).
!
! Variables containing the results of the Hapke model :
! - S : Shadowin function.
! - rS : Single-scattering contribution, corrected from the effective angles,
!	if existed, but not yet correct from the shadowing function.
! - rM : Multiple-scattering contribution, corrected from the effective angles,
!	if existed, but not yet correct from the shadowing function.
! - r : Reflectance corrected from the roughness ((rS+rM)*S).
!
! Other variables :
! - pi : Constant pi (3.14159...) defined from acos(-1.0).
!
! See also
! =========
! Module HapkeModel
! Module HapkeRough
! Module PrintMod
! 
! Compilation
! =============
! gfortran -c HapkeVar.f95
!
!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!

! Definition of the global variables.
IMPLICIT NONE
real :: w, tb, pi = acos(-1.0), BS0, BC0, hS, hC
real, dimension(:,:), allocatable :: i, e, phii, phie, phi, mu0, &
	mu, mu0p, mup, g, p, S, rS, rM, r
real, dimension(:), allocatable :: coeffs
integer :: ncoeffs, ndim1, ndim2

CONTAINS

!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
! Subroutine VarRecup
!
!  Recuperation of the parameters in the file './In/Param.asc' realised from the
!	program 'MakeVar' or modified by the hand.
!	The following parameters are taken : w, ncoeffs, coeff, the angles tb, i and
!	phii in degreesn the number of used angles for e and phie, in additiion to
!	their limits and the step in degrees.
!	Subroutines 'Allocation' and 'AnglesCalculation' are called in order to
!	allocate and calculate the matrix i, phii, e, phie, phi, g, mu0, mu and p.
!	Matrix mu0p, mup, rS, rM, r and S are also allocated but not calculated at
!	this time.
!
!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
SUBROUTINE VarRecup
	implicit none
	character(len=70) :: text
	integer :: ios, ic
	real :: ideg, phiideg, edeg_start, edeg_stop, edeg_delta, &
		phiedeg_start, phiedeg_stop, phiedeg_delta
	open(unit = 11, & 
		file = "./In/Param.asc", &
		form = "formatted", &
		access = "sequential", &
		action = "read", &
		position = "rewind", &
		iostat = ios)
	if ( ios /= 0 ) then
		print *,' '
		print *,"Error : File is not open."
		print *,' '
		STOP
	else
		! Recuperation of the variables.
		read(unit=11,fmt='(A18)') text
		read(unit=11,fmt='(A18)') text
		read(unit=11,fmt='(A29,F4.2)') text, w
		read(unit=11,fmt='(A16)') text
		read(unit=11,fmt='(A30,I2)') text, ncoeffs
		if(.not. allocated(coeffs)) then
			allocate(coeffs(ncoeffs))
		end if
		do ic=1,ncoeffs
			read(unit=11,fmt='(A23,F5.2)') text, coeffs(ic)
		end do
		read(unit=11,fmt='(A29,F5.2)') text, tb
		tb = tb*pi/180
		read(unit=11,fmt='(A1)') text
		read(unit=11,fmt='(A6)') text
		read(unit=11,fmt='(A21,F4.2)') text, BS0
		read(unit=11,fmt='(A24, F6.4)') text, hS
		read(unit=11,fmt='(A6)') text
		read(unit=11,fmt='(A21,F4.2)') text, BC0
		read(unit=11,fmt='(A24, F6.4)') text, hC

		read(unit=11,fmt='(A1)') text
		read(unit=11,fmt='(A31)') text
		read(unit=11,fmt='(A31)') text
		read(unit=11,fmt='(A19,F5.2)') text, ideg
		read(unit=11,fmt='(A22,F6.2)') text, phiideg

		read(unit=11,fmt='(A1)') text
		read(unit=11,fmt='(A32)') text
		read(unit=11,fmt='(A32)') text
		read(unit=11,fmt='(A30,I3)') text, ndim1
		read(unit=11,fmt='(A33,I3)') text, ndim2
		read(unit=11,fmt='(A1)') text
		read(unit=11,fmt='(A42,F5.2)') text, edeg_start
		read(unit=11,fmt='(A43,F5.2)') text, edeg_stop
		read(unit=11,fmt='(A45,F5.2)') text, edeg_delta
		read(unit=11,fmt='(A1)') text
		read(unit=11,fmt='(A42,F6.2)') text, phiedeg_start
		read(unit=11,fmt='(A43,F6.2)') text, phiedeg_stop
		read(unit=11,fmt='(A45,F6.2)') text, phiedeg_delta
		close(unit=11)

		! Matrix operations.
		! dim 1 : zenithal angles.
		! dim 2 : azimutal angles.
		call Allocation
		i = ideg*pi/180
		phii = phiideg*180/pi
		do ic=1,ndim1
			e(ic,:) = (edeg_start+(ic-1)*edeg_delta)*pi/180
		end do
		do ic=1,ndim2
			phie(:,ic) = (phiedeg_start+(ic-1)*phiedeg_delta)*pi/180
		end do
		call AnglesCalculation
	endif
END SUBROUTINE

!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
! Subroutine Allocation
!
!  Allocation of the matrix variables with dimension ndim1*ndim2 : i, phii, e, 
!	phie, phi, g, mu0, mu, mu0p, mup, g, p, rS, rM, r and S.
!
!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
SUBROUTINE Allocation
	if(.not. allocated(i)) then
		allocate(i(ndim1,ndim2))
	end if
	if(.not. allocated(e)) then
		allocate(e(ndim1,ndim2))
	end if
	if(.not. allocated(phii)) then
		allocate(phii(ndim1,ndim2))
	end if
	if(.not. allocated(phie)) then
		allocate(phie(ndim1,ndim2))
	end if
	if(.not. allocated(phi)) then
		allocate(phi(ndim1,ndim2))
	end if
	if(.not. allocated(mu0)) then
		allocate(mu0(ndim1,ndim2))
	end if
	if(.not. allocated(mu)) then
		allocate(mu(ndim1,ndim2))
	end if
	if(.not. allocated(mu0p)) then
		allocate(mu0p(ndim1,ndim2))
	end if
	if(.not. allocated(mup)) then
		allocate(mup(ndim1,ndim2))
	end if
	if(.not. allocated(g)) then
		allocate(g(ndim1,ndim2))
	end if
	if(.not. allocated(p)) then
		allocate(p(ndim1,ndim2))
	end if
	if(.not. allocated(S)) then
		allocate(S(ndim1,ndim2))
	end if
	if(.not. allocated(rS)) then
		allocate(rS(ndim1,ndim2))
	end if
	if(.not. allocated(rM)) then
		allocate(rM(ndim1,ndim2))
	end if
	if(.not. allocated(r)) then
		allocate(r(ndim1,ndim2))
	end if
END SUBROUTINE

!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
! Subroutine Deallocation
!
!  Deallocation of the matrix variables : i, phii, e, phie, phi, g, mu0, mu, 
!	mu0p, mup, g, p, rS, rM, r and S (allocated by the subroutine 'Allocation'),
!	but also coeffs (allocated by the subroutine 'VarRecup').
!
!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
SUBROUTINE Deallocation
	deallocate(coeffs)
	deallocate(i)
	deallocate(e)
	deallocate(phii)
	deallocate(phie)
	deallocate(phi)
	deallocate(mu0)
	deallocate(mu)
	deallocate(mu0p)
	deallocate(mup)
	deallocate(g)
	deallocate(p)
	deallocate(S)
	deallocate(rS)
	deallocate(rM)
	deallocate(r)
END SUBROUTINE

!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
! Function 'Pn'
!
!  Pn(n,x) applies the Legendre polynomial of degree n (degrees 0 to 5 are here
!	available) to the matrix x (ndim1*ndim2).
!
!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
FUNCTION Pn(n,x)
	implicit none
	integer, intent(in) :: n
	real, dimension(ndim1, ndim2), intent(in) :: x
	real, dimension(ndim1, ndim2) :: Pn
	select case(n)
	case(0)
		Pn = 1
	case(1)
		Pn = x
	case(2)
		Pn = 0.5*(3*(x**2)-1)
	case(3)
		Pn = 0.5*(5*(x**3)-3*x)
	case(4)
		Pn = 0.125*(35*(x**4)-30*(x**2)+3)
	case(5)
		Pn = 0.125*(63*(x**5)-70*(x**3)+15*x) 
	case default
		Pn = 0
		print *,"\nThe Legendre polynomial of degree",n,"is not ", &
			"available here. It won't be taken into account for the ", &
			"calculation of the phase function."
	end select
END FUNCTION

!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
! Subroutine AnglesCalculation
!
!  Calculation of the cosinus for the angles i -> mu0 and e -> mu, then 
!	calculation of the phase angles -> g and relative azimutal angles -> phi. 
!	The phase function is obtained by using the Legendre polynomials through the
!	function 'Pn'.
!
!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
SUBROUTINE AnglesCalculation 
	integer :: ic
	mu0 = cos(i)
	mu = cos(e)
	g = acos((mu*mu0)+(sin(e)*sin(i)*cos(phie-phii)))
	phi = acos(cos(phie-phii)) 
	! previously : acos( ( cos(g)-(mu0*mu) )/(sin(i)*sin(e)) )
	p = 1
	do ic=1,ncoeffs
		p = p + coeffs(ic)*Pn(ic,cos(g))
	end do
END SUBROUTINE

END MODULE
