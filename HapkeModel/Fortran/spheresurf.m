function [hfig,hcol] = spheresurf(theta,phi,color,varargin)
%SPHERESURF Representation of data from a semi-hemisphere surface on a
%spherical plot with colours.
%   SPHERESURF(THETA, PHI, COLOR) makes a plot using spherical coordinates
%   to show a hemispherical surface with values in matrix COLOR. The angle 
%   THETA is the colatitude (pi/2 - elevation) and the angle PHI is the 
%   azimuth.
%   THETA, PHI and COLOR must be vectors of the same size. THETA and PHI
%   in radians and the unity of COLOR depends on the user.
%
%   [HFIG,HCOL] = SPHERESURF(...) returns a handle to the figure in HFIG 
%   and a handle to the colorbar in HCOL.
%
%   Notes 
%   =====
%   VARARGIN argument: 'property','value' pairs that could modify plot 
%       characteristics. Property names must be specified as quoted 
%       strings. Be careful, property names and their values are not case 
%       sensitive. All properties are optional and briefly described
%       below :
%
%   - 'Radius'              Matrix with the same size as arguments THETA,
%                           PHI and COLOR in cas of the user don't want an
%                           hemisphere but a surface with radius as a
%                           function of angles.
%
%   - 'AngleUnit'           String that defines the angle unit for THETA 
%                           and PHI. 'rad' for radians (default) or 'deg' 
%                           for degrees.
%
%   - 'View'                Vector to precise the view point of the plot
%                           define as [azimuth, elevation]. Be carefull,
%                           the elevation here is pi/2 - colatitude.
%
%   - 'DeltaRange'          Scalar that gives the distance, in degrees, 
%                           between two reference circles in range (THETA).
%                           If not given, the default value, 30�, is taken.
%
%   - 'DeltaRangeOffset'    Scalar that gives the offset, in degrees, i.e
%                           the value for the first circle plotted (THETA).
%                           If not given, the default value, 0�, is taken.
%
%   - 'DeltaAzimuth'        Scalar that gives the distance, in degrees, 
%                           between two reference values in azimuth (PHI).
%                           If not given, the default value, 30�, is taken.
%
%   - 'DeltaAzimuthOffset'  Scalar that gives the offset, in degrees, i.e
%                           the first value to be represented.
%                           If not given, the default value, 0�, is taken.
%
%   - 'RangeValues'         Scalar or vector that give the exact values, in
%                           degrees, of the different reference cicrcles in
%                           range (THETA). This parameter replace
%                           'DeltaRange' and 'DeltaRangeOffset'.
%
%   - 'AzimuthValues'       Scalar or vector that give the exact values, in
%                           degrees, of the different reference values in
%                           azimuth (PHI). This parameter replace
%                           'AzimuthRange' and 'AzimuthRangeOffset'.
%
%   - 'AzimuthLines'        Scalar or vector that give the exact values, in
%                           degrees, of the different reference lines in
%                           azimuth (PHI) on the hemisphere. If not given,
%                           this parameter is 0:60:360;
%
%   - 'Phi_RangeNumbers'    Scalar that gives the azimuth value (PHI)
%                           corresponding to the direction where values of
%                           the diffents reference circles (THETA) are
%                           written. 
%                           If not given, the default value, 75�, is taken.
%
%   - 'CAxis'               Vector giving the range [min max] of the
%                           colorbar. If not given, the limit values are
%                           chosen automatically.
%
%   - 'Colorbar'            String that defines presence or no of a
%                           colorbar. 'yes' if user wants a colorbar or 
%                           'no' (default) if you don't want.
%
%   - 'ColorbarTitle'       String to add a title on the color bar and thus
%                           precise the values represented.
%
%   - 'Title'               String to add a title on the figure.
%
%   - 'Star'                Vector giving the direction [THETA PHI] of the
%                           source of light, represented by a star (angles
%                           are in degrees here).
%
%   - 'StarDist'            Scalar that give the factor f which represents
%                           the distance (f*R) between the star plotted and
%                           the center of the hemisphere. Default value is
%                           1.5.
%
%   - 'Profile'             Scalar containing the azimuthal angle PHI of
%                           the profile to represent, or cell containing
%                           first this scalar and then a colored vector of
%                           3 values between 0 and 1 [R, G, B]. The
%                           argument is thus PHI or {PHI,[R G B]}. Note
%                           that the profile will covered the data with
%                           azimuthal angle equals to PHI and PHI+180 (The
%                           angle have to be in degrees here) and the 
%                           default color is white. Another scalar can be
%                           added to represents the linewidth.
%
%   - 'BlackOrWhite'        String containing the name of the background
%                           color of the figure. Some of the lines of
%                           writing will be in the opposite color
%                           obviously. The default value is 'White' but it
%                           could be also 'Black'.
%
%   - 'Print'               String containing the name of the file to save
%                           the figure. It will be saved within the current
%                           folder as a PNG file with a 500 Dots-per-inch 
%                           resolution defined in this function.
%
%   Examples
%   ========
%   clear all; close all; clc;
%   theta_i = 35; phi_i = 0;
%   theta_e = 0:90; phi_e = 0:360;
%   [theta_e, phi_e] = meshgrid(theta_e*pi/180, phi_e*pi/180);
%   theta_i = ones(size(theta_e))*theta_i*pi/180;
%   phi_i = ones(size(phi_e))*phi_i*pi/180;
%   w = 1;
%   b = 1; c = 0.25;
%   B0 = 1; h = 0.2;
%   r = Hapke1981_r(theta_i, phi_i, theta_e, phi_e, w, b, c, B0, h);
%   rS = Hapke1981_rS(theta_i, phi_i, theta_e, phi_e, w, b, c, B0, h);
%   rM = Hapke1981_rM(theta_i, theta_e, w);
%   spheresurf(theta_e,phi_e,rS,'AzimuthValues',[330 0 30 60 90 120 150],...
%       'Star',[35 0],'View',[60 30],'StarDist',1.8,...
%       'BlackOrWhite','Black','Colorbar','yes',...
%       'ColorbarTitle','Reflectance de simple diffusion',...
%       'CAxis',[0 0.2],'Print','!testblack2');
%
%   See also
%   ========
%   MESHGRID, SPH2CART, SURFACE, SHADING, COLORBAR, CAXIS, PLOT, LINE
%   PLOT3, AXIS, LINE
%   POLARC
%
%   Versions
%   ========
%   1   (Jason Champion - 2013/03/28) First test for the feasability of
%       this function.
%   1.1 (Jason Champion - 2013/03/29) Second test and first developpement
%       with only the three necessary arguments.
%   2.0 (Jason Champion - 2013/03/30) First developpement as a function
%       with optional arguments, addition of the documentation and
%       comments.
%
%   Copyright free function originally made by Jason Champion.
%   Revision: 2
%   Date: 2013/03/30 12:45

%% OPTIONAL ARGUMENTS AT THE START %%
%===================================%
% Default values
deltarange = 30;
deltarangeoffset = 0;
deltaazimuth = 30;
deltaazimuthoffset = 0;
rangevalues = [];
azimuthvalues = [];
azimuthlines = 0:60:360;
%phi_rangenumbers = 75;
radius = ones(size(color));
colorb_yorn = 0;
stardist = 1.5;
viewvect = [];
blackorwhite = 0;
if nargin > 3
    N = length(varargin);
    for j=1:2:N
        switch varargin{j}
            case 'View'
                vect = varargin{j+1};
                viewvect = [vect(1)+90,vect(2)];
            case 'Radius'
                radius = varargin{j+1};
            case 'AngleUnit'
                if strcmp(varargin{j+1},'deg')
                    % conversion in radians
                    theta = theta*pi/180;
                    phi = phi*pi/180;
                elseif strcmp(varargin{j+1},'rad')
                    % nothing to do
                else
                    warning(['Argument with ''AngleUnit'' could ',...
                        'only be ''rad'' if angles are in radians ',...
                        '(as default) or ''deg'' if they are in ',...
                        'degrees.']); %#ok<WNTAG>
                end
            case 'Colorbar'
                if strcmp(varargin{j+1},'yes')
                    colorb_yorn = 1;
                elseif strcmp(varargin{j+1},'no')
                    % nothing to do
                else
                    warning(['Argument with ''Colorbar'' could ',...
                        'only be ''yes'' if you want to add one ',...
                        'or ''no'' (as default) if you don''t ',...
                        'want.']); %#ok<WNTAG>
                end
            case 'DeltaRange'
                deltarange = varargin{j+1};
            case 'DeltaRangeOffset'
                deltarangeoffset = varargin{j+1};
            case 'DeltaAzimuth'
                deltaazimuth = varargin{j+1};
            case 'DeltaAzimuthOffset'
                deltaazimuthoffset = varargin{j+1};
            case 'RangeValues'
                rangevalues = varargin{j+1};
            case 'AzimuthValues'
                azimuthvalues = varargin{j+1};
            case 'AzimuthLines'
                azimuthlines = varargin{j+1};
%             case 'Phi_RangeNumbers'
%                 phi_rangenumbers = varargin{j+1};
            case 'StarDist'
                stardist = varargin{j+1};
            case 'BlackOrWhite'
                if strcmp(varargin{j+1},'Black')
                    blackorwhite = 1;
                elseif strcmp(varargin{j+1},'White')
                    % nothing to do
                else
                    warning(['Argument with ''BlackOrWhite'' could ',...
                        'only be ''Black'' if you want a black ',...
                        'background ',...
                        'or ''White'' (as default) if you want a ',...
                        'white background']); %#ok<WNTAG>
                end
        end
    end
end

%% PLOTTING DATA %%
%=================%
if blackorwhite == 0
    hfig = figure('color','white');
    colvect = [0 0 0];
elseif blackorwhite == 1
    hfig = figure('color','black');
    colvect = [1 1 1];
end
[x,y,z]=sph2cart(phi,(pi/2)-theta,radius);
if ~isempty(viewvect)
    view(viewvect(1), viewvect(2))
end
surface(x,y,z,color);

% Modifications
shading flat
set(gca,'Visible','off')
axis equal
if colorb_yorn == 1
    hcol = colorbar('location','southoutside');
    set(hcol,'fontsize',15,'box','on');
else
    hcol = [];
end

% Definition of some parameters
radmax = max(radius(:));
col = caxis;

% Adding reference values and lines in azimuth (representing phi)
if ~isempty(azimuthvalues)
    vect = azimuthvalues;
else
    vect = deltaazimuthoffset:deltaazimuth:360;
end
for i=vect;
    line([0 cosd(i)*radmax],...
        [0 sind(i)*radmax],...
        [0 0],...
        'linestyle','-.','color',[0 0 0])
    if i<360
        text(1.15*(cosd(i)*max(radius(:))),...
            1.15*sind(i)*radmax,[num2str(i) '�'],...
            'HorizontalAlignment', 'center', 'VerticalAlignment', 'middle',...
            'fontsize',15,'color',colvect);
    end
end

vect = azimuthlines;
for i=vect;
    t=0:90;
    [t,i2]=meshgrid(t,[i i]);
    [xi,yi,zi] = sph2cart(i2*pi/180,pi/2-t*pi/180,1.001*radmax);
    surface(xi,yi,zi,'linewidth',1,'linestyle','-.')
end

% Adding reference lines in colatitude/elevation (representing theta)
if ~isempty(rangevalues)
    vect = rangevalues;
else
    vect = deltarangeoffset:deltarange:90;
end
for i=vect;
    t=0:360;
    [t,i2]=meshgrid(t,[i i]);
    [xi,yi,zi] = sph2cart(t*pi/180,i2*pi/180,1.001*radmax);
    surface(xi,yi,zi,'linewidth',1)
%     if i>0 && i<90
%         text((i/range+0.06)*max(theta(:))*cosd(phi_rangenumbers),...
%            (i/range+0.06)*max(theta(:))*sind(phi_rangenumbers),...
%            max(magnitude(:)),[num2str(i) '�'],...
%            'HorizontalAlignment', 'center', 'VerticalAlignment', ...
%            'middle','fontsize',10);
%     end
end

%% OPTIONAL ARGUMENTS AT THE END %%
%=================================%
% last modifications
colormap(jet(1000))
caxis(col);

if nargin > 3
    N = length(varargin);
    for j=1:2:N
        switch varargin{j}
            case 'ColorbarTitle'
                set(get(hcol,'XLabel'),'String',varargin{j+1},...
                    'fontsize',15,'color',colvect);
            case 'CAxis'
                caxis(varargin{j+1})
            case 'Title'
                title(varargin{j+1});
            case 'Star'
                vect = varargin{j+1};
                theta_i = vect(1);
                phi_i = vect(2);
                [xsource,ysource,zsource]=sph2cart(phi_i*pi/180,...
                    pi/2-theta_i*pi/180,stardist*radmax);
                line([0 xsource],[0 ysource],[0 zsource],'linestyle',':',...
                    'linewidth',2,'color',colvect)
                hold on
                [xsource,ysource,zsource]=sph2cart(phi_i*pi/180,...
                    pi/2-theta_i*pi/180,stardist*radmax);
                plot3(xsource,ysource,zsource,'marker','p',...
                    'markerfacecolor','y',...
                    'markeredgecolor','r','markersize',15)
            case 'Print'
                if blackorwhite==1
                    set(gcf, 'InvertHardCopy', 'off');
                end
                print('-dpng','-r500',[varargin{j+1} '.png'])
        end
    end
end

end