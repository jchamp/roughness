PROGRAM Makezone
! gfortran -c PrintMod.f95 MakeObj.f95
! gfortran PrintMod.o MakeObj.o Makezone.f95 -o Makezone.exe

USE PrintMod
USE MakeObj

IMPLICIT NONE
integer :: Nx, Ny, x1, x2, y1, y2, ios, ic, length
real, dimension(:,:), allocatable :: x,y,z
character(len=50) :: filename, poub, myfmt

! Lecture fichier ./In/info.asc pour recuperer le nom.
open(unit = 11, & 
	file = './In/info.asc', &
	form = "formatted", &
	access = "sequential", &
	action = "read", &
	position = "rewind", &
	iostat = ios)
if ( ios /= 0 ) then
	print *,"\n PROBLEME A L'OUVERTURE\n"
	STOP
else
	read(unit=11,fmt='(A50)') filename
	close(unit=11)
endif

! Lecture fichier ./Out/"filename".asc pour recuperer la taille.
open(unit = 12, & 
	file = './Out/'//trim(filename)//'.asc', &
	form = "formatted", &
	access = "sequential", &
	action = "read", &
	position = "rewind", &
	iostat = ios)
if ( ios /= 0 ) then
	print *,"\n PROBLEME A L'OUVERTURE\n"
	STOP
else
	read(unit=12,fmt='(A50)') poub
	read(unit=12,fmt='(A50)') poub
	read(unit=12,fmt='(A50)') poub
	read(unit=12,fmt='(A50)') poub
	read(unit=12,fmt='(A50)') poub
	read(unit=12,fmt='(I5)') x1
	read(unit=12,fmt='(I5)') x2
	read(unit=12,fmt='(I5)') y1
	read(unit=12,fmt='(I5)') y2
	Nx = x2-x1+1
	Ny = y2-y1+1
	close(unit=12)
endif

length = 1
do while (Nx/(10**length) .ge. 1)
	length = length + 1
end do

! Recuperation des variables x, y, z.
if (.not. allocated(x)) then
	allocate(x(Ny,Nx))
end if
if (.not. allocated(y)) then
	allocate(y(Ny,Nx))
end if
if (.not. allocated(z)) then
	allocate(z(Ny,Nx))
end if
write(poub,'("(A1,I" ,I1, ",A6)")') length
write(myfmt,poub) '(',Nx,'F11.7)' 
x = RecupMat('./Out/x.asc',Ny,Nx,myfmt)
y = RecupMat('./Out/y.asc',Ny,Nx,myfmt)
z = RecupMat('./Out/z.asc',Ny,Nx,myfmt)

! Creation des fichiers .obj et -geom.lxo.
call MakeObjAndLxoFiles(x,y,z,Ny,Nx,'./Out/'//trim(filename),'rand')

deallocate(x)
deallocate(y)
deallocate(z)

END PROGRAM
