clear all
close all
clc

addpath('./In/');
fid=fopen('info.asc');
list = textscan(fid,'%s');

name = char(list{1});

% Fichier asci.
fidasc = fopen([name '.asc']);
dataasc = textscan(fidasc,'%s');
dataasc = dataasc{1};
Nx = str2num(cell2mat(dataasc(5))); %#ok<*ST2NM>
Ny = str2num(cell2mat(dataasc(6)));
dx = str2num(cell2mat(dataasc(7)));
dy = str2num(cell2mat(dataasc(8)));

% Fichier tif.
x=1:Nx;
y=1:Ny;
im = Tiff(['./In/' name '.tif'],'r');
imData=im.read();
imagesc(x,y,imData);
axis equal
axis([min(x(:)) max(x(:)) min(y(:)) max(y(:))])
colorbar
colormap(bone(500))
%[xlim,ylim] = ginput;
%xlim=fix(xlim);
%ylim=fix(ylim);	
[xc,yc] = ginput;
xlim=[fix(xc)-415 fix(xc)+415];
ylim=[fix(yc)-415 fix(yc)+415];
imDataEx = imData(ylim(1):ylim(2),xlim(1):xlim(2));
im.close();
colormap(bone(500))
line([xlim(1) xlim(2)],[ylim(1) ylim(1)],'color','r','linewidth',2);
line([xlim(1) xlim(2)],[ylim(2) ylim(2)],'color','r','linewidth',2);
line([xlim(1) xlim(1)],[ylim(1) ylim(2)],'color','r','linewidth',2);
line([xlim(2) xlim(2)],[ylim(1) ylim(2)],'color','r','linewidth',2);
title(['MNT de la surface ' name(1:end-4) ...
	' et zone (rouge) extraite en .obj et -geom.lxo'],...
	'interpreter','none');
set(gca,'box','on');
set(gca,'Xtick',[]);
set(gca,'Ytick',[]);
print('-dpng','-r700',['./Out/' name '_extr.png'])
close all

x=1:size(imDataEx,2);
y=1:size(imDataEx,1);
x=x*dx;
y=y*dy;
x=x-mean(x);
% inversion en meme temps donc pas y=y-mean(y); mais :
y=-(y-mean(y));
%imDataEx=imDataEx-min(imDataEx(:));
imDataEx=imDataEx-mean(imDataEx(:));
imagesc(x,y,imDataEx)
set(gca,'ydir','normal');
axis equal
axis([min(x(:)) max(x(:)) min(y(:)) max(y(:))])
colorbar
colormap(bone(500))
set(gca,'box','on');
title(['Zone extraire du MNT ' name(1:end-4),...
	' avec de nouvelles coordonnees centrees'],...
	'interpreter','none');
print('-dpng','-r700',['./Out/' name '_extr2.png'])
close all

% Fichier asci modifiee.
eval(['system(''cp ./In/' name '.asc ./Out/' name '.asc'');'])
fidasc2 = fopen(['./Out/' name '.asc'],'a+');
fprintf(fidasc2,'%i',xlim(1));
fprintf(fidasc2,'\n%i',xlim(2));
fprintf(fidasc2,'\n%i',ylim(1));
fprintf(fidasc2,'\n%i',ylim(2));
	
% Création des donnees.
[x,y]=meshgrid(single(x),single(y));
z = single(imDataEx);
str={'x','y','z'};
disp([char(10) 'Enregistrement des donnees de la zone...'])
for ic = 1:3
	disp(['     - ' str{ic} ' : ./Out/' str{ic} '.asc'])
	eval(['dlmwrite([''./Out/'' str{ic} ''.asc''],' str{ic} ',''delimiter'','''',''precision'',''%11.7f'');'])
	% Pas de delimiter car on doit lire en fortran ensuite.
end

% Appel de fortran.
system('make all');
disp([char(10) 'Passage en Fortran pour la creation des fichiers .obj et -geom.lxo...'])
system('./Makezone.exe');
disp([char(10) 'Passage en Fortran pour le traitement des pentes...'])
system('./Slopes.exe');
disp('Done!')
