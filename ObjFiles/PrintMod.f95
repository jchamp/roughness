MODULE PrintMod
!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
!
! ==============================================================================
!                   	    Module PrintMod
!                Jason Champion - 2013/04/12-2013/04/16
! ==============================================================================
!
! Introduction:
! ===============
! Ce module contient des fonctions concernant l'affichage de donnees dans le 
! terminal ou l'enregistrement de celles-ci dans un fichier.
!
! Il contient 5 fonctions/subroutines :
! - PrintMat(mat,nl,nc)
! - PrintMatExt(mat,nl,nc,lmin,lmax,cmin,cmax)
! - PrintMat_Text(mat,nl,nc,text)
! - PrintMatExt_Text(mat,nl,nc,lmin,lmax,cmin,cmax,text)
! - WriteOut(mat,nl,nc,namefile)
! - RecupMat(filename,nl,nc,myfmt)
! 
! Compilation
! =============
! g95 -c PrintMod.f95
!
!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!

CONTAINS

!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
! Subroutine 'PrintMat(mat,nl,nc)'
!
!  Affichage de la matrice 'mat' de dimension nl*nc dans le terminal avec un
!	saut de ligne avant.
!
!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
SUBROUTINE PrintMat(mat,nl,nc)
	implicit none
	integer :: il
	integer, intent(in) :: nl, nc
	real, dimension(nl,nc), intent(in) :: mat
	print *,
	do il=1,nl,1
		print *,mat(il,:)
	end do
END SUBROUTINE

!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
! Subroutine 'PrintMatExt(mat,nl,nc,lmin,lmax,cmin,cmax)'
!
!  Affichage, dans le terminal, de la portion de la matrice 'mat', de dimension 
!	nl*nc, compris entre les indices lmin:lmax en ligne et cmin:cmax en 
!	colonnes. Un saut de ligne avant est effectue avant.
!
!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
SUBROUTINE PrintMatExt(mat,nl,nc,lmin,lmax,cmin,cmax)
	implicit none
	integer :: il
	integer, intent(in) :: nl, nc, lmin, lmax, cmin, cmax
	real, dimension(nl,nc), intent(in) :: mat
	print *,
	do il=lmin,lmax,1
		print *,mat(il,cmin:cmax)
	end do
END SUBROUTINE

!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
! Subroutine 'PrintMat_Text(mat,nl,nc,text)'
!
!  Affichage de la matrice 'mat' de dimension nl*nc dans le terminal avec un 
!	saut de ligne avant et une phrase entree par l'utilisateur ('text').
!
!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
SUBROUTINE PrintMat_Text(mat,nl,nc,text)
	implicit none
	integer :: il
	integer, intent(in) :: nl, nc
	real, dimension(nl,nc), intent(in) :: mat
	character (len=*),intent(in) :: text
	print *,
	print *, text
	do il=1,nl,1
		print *,mat(il,:)
	end do
END SUBROUTINE

!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
! Subroutine 'PrintMatExt_Text(mat,nl,nc,lmin,lmax,cmin,cmax,text)'
!
!  Affichage, dans le terminal, de la portion de la matrice 'mat', de dimension 
!	nl*nc, compris entre les indices lmin:lmax en ligne et cmin:cmax en 
!	colonnes. Avant cet affichage, un saut de ligne avant est effectue et 
!	une phrase entree par l'utilisateur ('text') est affichee.
!
!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
SUBROUTINE PrintMatExt_Text(mat,nl,nc,lmin,lmax,cmin,cmax,text)
	implicit none
	integer :: il
	integer, intent(in) :: nl, nc, lmin, lmax, cmin, cmax
	real, dimension(nl,nc), intent(in) :: mat
	character (len=*),intent(in) :: text
	print *,
	print *, text
	do il=lmin,lmax,1
		print *,mat(il,cmin:cmax)
	end do
END SUBROUTINE

!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
! Subroutine 'WriteOut(mat,nl,nc,namefile)'
!
!  Enregistrement de la matrice 'mat' de dimension nl*nc dans un fichier dont le
!	chemin, le nom et l'extension sont precises dans la variable 'namefile'.
!	L'ecriture remplace l'ancien fichier s'il existe et se fait de maniere
!	sequentielle et formatee.
!
!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
SUBROUTINE WriteOut(mat,nl,nc,namefile)
	implicit none
	integer :: il, ios, length
	integer, intent(in) :: nl, nc
	real, dimension(nl,nc), intent(in) :: mat
	character (len=*),intent(in) :: namefile
	character (len=40) :: myfmt, str
	length=1
	if (nc>9) then
		length=length+1
		if (nc>99) then
			length=length+1
			if (nc>999) then
				length=length+1
			end if
		end if
	end if
	!Affiche ("(A2,",IX,"F10.3,A7)") avec X = length au format I1
	!WRITE(str,'("(""(A2,"",I", I1,",""F10.3,A7)"")")') length 
	!WRITE(myfmt,str) nc
	!Affiche "(",IX,"F10.3,A7)") avec X = length au format I1
	!WRITE(str,'("(""(A2,"",I", I1,",""F15.10,A7)"")")') length 
	! Je veux : 
	! myfmt = (<nc>F10.3) donc write(myfmt,'( "(",I3,"F10.3)" )') nc 
	! Mais je ne sais pas si c'est I3 ou I2,I4 par exemple donc :
	! write(myfmt,str) nc avec str = ("(",I3,"F10.3)") 
	! mais avec I3 definit par la variable length d'ou :
	! write(str,'(" (""("",I", I1 ,",""F10.3)"") ")') length 
	! (I1 ou I5, il faut que ce soit assez)
	write(str,'(" (""("",I", I3 ,",""F15.10)"") ")') length
	write(myfmt,str) nc
	open(unit = 12, & 
		file = namefile, &
		form = "formatted", &
		access = "sequential", &
		action = "write", &
		status = "replace", &
		position = "rewind", &
		iostat = ios)
	if ( ios /= 0 ) then
		print *,"\n PROBLEME A L'OUVERTURE\n"
		STOP
	else
		do il=1,nl,1
			write(unit=12,fmt=myfmt) mat(il,:)
		end do
		close(unit=12)
	endif
END SUBROUTINE

!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
! Fonction 'RecupMat(filename,nl,nc,myfmt)'
!
!  Fonction permettant d'aller recuperer la matrice de taille nl*nc enregistree
!	par Matlab et la fonction dlmwrite (avec aucun delimiter) dans le 
!	fichier 'filename'.
!
!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
FUNCTION RecupMat(filename,nl,nc,myfmt)
	implicit none
	integer, intent(in) :: nl, nc
	character(len=*), intent(in) :: filename, myfmt
	real, dimension(nl,nc) :: RecupMat
	integer :: il, ios
	open(unit = 13, & 
		file = filename, &
		form = "formatted", &
		access = "sequential", &
		action = "read", &
		position = "rewind", &
		iostat = ios)
	if ( ios /= 0 ) then
		print *,"\n PROBLEME A L'OUVERTURE\n"
		STOP
	else
		do il=1,nl,1
			read(unit=13,fmt=myfmt) RecupMat(il,:)
		end do
		close(unit=13)
	endif
END FUNCTION

END MODULE
