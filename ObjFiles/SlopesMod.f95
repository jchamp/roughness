MODULE SlopesMod

CONTAINS

FUNCTION ProdVect(ab,ac)
	implicit none
	real, dimension(3), intent(in) :: ab, ac
	real, dimension(3) :: ProdVect
	ProdVect(1) = ab(2)*ac(3)-ab(3)*ac(2)
	ProdVect(2) = ab(3)*ac(1)-ab(1)*ac(3)
	ProdVect(3) = ab(1)*ac(2)-ab(2)*ac(1)
END FUNCTION

FUNCTION Norm(v,ndim)
	integer, intent(in) :: ndim
	real, dimension(ndim), intent(in) :: v
	real :: Norm
	integer :: ic
	Norm = 0.0
	do ic=1,ndim
		Norm = Norm + v(ic)**2
	end do
	Norm = sqrt(Norm)
END FUNCTION

FUNCTION SlopeAngle(a,b,c)
	implicit none
	real, dimension(3), intent(in) :: a, b, c
	real, dimension(3) :: ab, ac, n
	real :: SlopeAngle, pi
	pi = acos(-1.0)
	ab=b-a
	ac=c-a
	n = ProdVect(ab,ac)
	n = n/Norm(n,3)
	SlopeAngle = acos(abs(n(3)))
END FUNCTION

END MODULE
