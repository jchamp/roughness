PROGRAM Slopes
! gfortran -c PrintMod.f95 SlopesMod.f95
! gfortran PrintMod.o SlopesMod.o Slopes.f95 -o Slopes.exe

USE PrintMod
USE SlopesMod

implicit none
real, dimension(:), allocatable :: x, y, z, sltab
real, dimension(3) :: a, b, c
real :: slmean, tanslmean
integer :: ios, Nx, Ny, N, ic, ind1, ind2, ind3
character(len=50) :: filename, poub

! Lecture fichier ./In/info.asc pour recuperer le nom.
open(unit = 11, & 
	file = './In/info.asc', &
	form = "formatted", &
	access = "sequential", &
	action = "read", &
	position = "rewind", &
	iostat = ios)
if ( ios /= 0 ) then
	print *,"\n PROBLEME A L'OUVERTURE\n"
	STOP
else
	read(unit=11,fmt='(A50)') filename
	close(unit=11)
endif

! Lecture du fichier ./Out/"filename".obj pour recuperer les facettes.
open(unit = 12, & 
	file = './Out/'//trim(filename)//'.obj', &
	form = "formatted", &
	access = "sequential", &
	action = "read", &
	position = "rewind", &
	iostat = ios)
if ( ios /= 0 ) then
	print *,"\n PROBLEME A L'OUVERTURE\n"
	STOP
else
	read(unit=12,fmt='(A6,I6)') poub, Ny
	read(unit=12,fmt='(A6,I6)') poub, Nx
	N = Ny*Nx
	if (.not. allocated(x)) then
		allocate(x(N))
	end if
	if (.not. allocated(y)) then
		allocate(y(N))
	end if
	if (.not. allocated(z)) then
		allocate(z(N))
	end if
	read(unit=12,fmt='(A50)') poub
	do ic=1,N
		read(unit=12,fmt='(A2,F11.7,A1,F11.7,A1,F11.7)') &
			poub, x(ic), poub, y(ic), poub, z(ic)
	end do
	read(unit=12,fmt='(A50)') poub
	read(unit=12,fmt='(A50)') poub
	read(unit=12,fmt='(A50)') poub

	N = (Ny-1)*(Nx-1)*2
	if (.not. allocated(sltab)) then
		allocate(sltab(N))
	end if
	open(unit = 13, & 
		file = './Out/'//trim(filename)//'_slopes.asc', &
		form = "formatted", &
		access = "sequential", &
		action = "write", &
		status = "replace", &
		position = "rewind", &
		iostat = ios)
	if ( ios /= 0 ) then
		print *,"\n PROBLEME A L'OUVERTURE\n"
		STOP
	else
		slmean=0
		tanslmean = 0
		do ic=1,N
			read(unit=12, fmt='(A2,I7,A1,I7,A1,I7)') & 
				poub, ind1, poub, ind2, poub, ind3
			a = (/x(ind1),y(ind1),z(ind1)/)
			b = (/x(ind2),y(ind2),z(ind2)/)
			c = (/x(ind3),y(ind3),z(ind3)/)
			sltab(ic) = SlopeAngle(a,b,c)
			tanslmean = atan((tan(tanslmean)*(ic-1) + (tan(sltab(ic))))/ic)
			slmean = (slmean*(ic-1) + (sltab(ic)))/ic 
		end do
		write(unit=13,fmt='(A21,F9.7)') & 
			'# mean slope (rad) : ', slmean
		write(unit=13,fmt='(A21,F7.4)') & 
			'# mean slope (deg) : ', slmean*180/acos(-1.0)
		write(unit=13,fmt='(A,F9.7)') & 
			'# atan mean slope (rad) : ', tanslmean
		write(unit=13,fmt='(A,F7.4)') & 
			'# atan mean slope (deg) : ', tanslmean*180/acos(-1.0)
		write(unit=13,fmt='(A1)') ' '

		do ic=1,N
			write(unit=13,fmt='(F9.7)') sltab(ic) 
		end do
		close(unit=13)
	end if
	close(unit=12)
endif

END PROGRAM
