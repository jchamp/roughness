#!/bin/bash

echo -e "\nSauvegarde dans ../Out/Save/$1/ de : "

cd ./Out/

if ! [ -d Save ]; then
	mkdir Save
fi

cd ./Save/
if ! [ -d $1 ]; then
	mkdir $1
fi
cd ..

for var in `ls *.asc`
do 
	long="${#var}"	
	varnew=${var:0:$long-4}"_"$1".asc"
	cp $var ./Save/$1/$varnew
	echo -e "  - "$varnew
done

for var in `ls *.png`
do 
	long="${#var}"	
	varnew=${var:0:$long-4}"_"$1".png"
	cp $var ./Save/$1/$varnew
	echo -e "  - "$varnew
done

for var in `ls *.obj`
do 
	long="${#var}"	
	varnew=${var:0:$long-4}"_"$1".obj"
	cp $var ./Save/$1/$varnew
	echo -e "  - "$varnew
done

for var in `ls *.lxo`
do 
	long="${#var}"	
	varnew=${var:0:$long-9}"_"$1"-geom.lxo"
	cp $var ./Save/$1/$varnew
	echo -e "  - "$varnew
done

rm *.*

cd ..
