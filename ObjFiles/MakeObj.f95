MODULE MakeObj

CONTAINS 

SUBROUTINE init_random_seed()
            INTEGER :: i, n, clock
            INTEGER, DIMENSION(:), ALLOCATABLE :: seed
          
            CALL RANDOM_SEED(size = n)
            ALLOCATE(seed(n))
          
            CALL SYSTEM_CLOCK(COUNT=clock)
          
            seed = clock + 37 * (/ (i - 1, i = 1, n) /)
            CALL RANDOM_SEED(PUT = seed)
          
            DEALLOCATE(seed)
END SUBROUTINE

SUBROUTINE MakeObjFile(x,y,z,ndim1,ndim2,filename,selectdiag)
	character(len=*), intent(in) :: filename, selectdiag	
	integer, intent(in) :: ndim1, ndim2
	real, dimension(ndim1,ndim2), intent(in) :: x, y, z
	integer, dimension(ndim1,ndim2) :: n
	real, dimension(ndim1-1,ndim2-1) :: r
	integer :: il, ic, nn
	open(unit = 11, & 
		file = filename, &
		form = "formatted", &
		access = "sequential", &
		action = "write", &
		status = "replace", &
		position = "rewind", &
		iostat = ios)
	if ( ios /= 0 ) then
		print *,"\n PROBLEME A L'OUVERTURE\n"
		STOP
	else
		! Commentaires du fichier .obj
		write(unit=11,fmt='(A6,I6)') &
			'# y : ',ndim1
		write(unit=11,fmt='(A6,I6)') &
			'# x : ',ndim2
		write(unit=11,fmt='(A1)') ' '
		! Coordonnees (x,y,z) des points.
		nn = 0
		do il=1,ndim1,1
			do ic=1,ndim2,1
				nn = nn + 1
				write(unit=11,fmt='(A2,F11.7,A1,F11.7,A1,F11.7)') &
					'v ',x(il,ic),' ',y(il,ic),' ',z(il,ic)
				n(il,ic) = nn
			end do
		end do
		!write(unit=11,fmt='(A1)') ' '
		! Coordonnees de texture.
		!do il=1,ndim1,1
		!	do ic=1,ndim2,1
		!		write(unit=11,fmt='(A3,F12.6,A1,F12.6)') 'vt ', &
		!			(il-1.0)/(ndim1-1.0),' ', &
		!			(ic-1.0)/(ndim2-1.0)
		!	end do
		!end do
		! Facettes.
		if (selectdiag .eq. 'NESO') then
			print *, "Case : NESO."
			r = 1
		else if (selectdiag .eq. 'NOSE') then
			print *, "Case : NOSE."
			r = 2
		else if (selectdiag .eq. 'rand') then
			print *, "Case : rand."
			call init_random_seed()
 			call random_number(r)
			where (r>=0.5)
				r = 2
			elsewhere
				r = 1
			end where
		else
			print *, "ERROR : The lase argument of the ", &
				"subroutine 'MakeOlbjFile' should be either ", &
				" NOSE, NESO or rand."
			STOP
		end if
			
		write(unit=11,fmt='(A1)') ' '
		write(unit=11,fmt='(A6)') 'g mesh'
		write(unit=11,fmt='(A1)') ' '
		do il=1,ndim1-1,1
			do ic=1,ndim2-1,1
				if (r(il,ic)==1) then
					write(unit=11, fmt= &
						'(A2,I7,A1,I7,A1,I7)') & 
						'f ',n(il,ic),' ',n(il+1,ic+1), &
						' ',n(il+1,ic)
					write(unit=11, fmt= &
						'(A2,I7,A1,I7,A1,I7)') & 
						'f ',n(il,ic+1),' ',n(il+1,ic+1), &
						' ',n(il,ic)
				else
					write(unit=11, fmt= &
						'(A2,I7,A1,I7,A1,I7)') & 
						'f ',n(il,ic+1),' ',n(il+1,ic), &
						' ',n(il,ic)
					write(unit=11, fmt= &
						'(A2,I7,A1,I7,A1,I7)') & 
						'f ',n(il+1,ic+1),' ',n(il+1,ic), &
						' ',n(il,ic+1)
				end if
			end do
		end do	
		write(unit=11,fmt='(A1)') ' '
		write(unit=11,fmt='(A1)') 'g'
		close(unit=11)
	endif
END SUBROUTINE MakeObjFile

FUNCTION I2S(I)
	integer, intent(in) :: I
	character(len=100) :: I2S
	integer :: Ilength, Ii
	character(len=100) :: MyFmt
	Ii = I
	Ilength = 1
	DO WHILE (Ii .ge. 10)
		Ii=Ii/10
		Ilength = Ilength+1
	END DO
	write(MyFmt,fmt='(A,I1,A)') "(I",Ilength,")"
	write(I2S,fmt=MyFmt) I
END FUNCTION I2S

SUBROUTINE MakeObjAndLxoFiles(x,y,z,ndim1,ndim2,filename,selectdiag)
	character(len=*), intent(in) :: filename, selectdiag	
	integer, intent(in) :: ndim1, ndim2
	real, dimension(ndim1,ndim2), intent(in) :: x, y, z
	integer, dimension(ndim1,ndim2) :: n
	real, dimension(ndim1-1,ndim2-1) :: r
	integer :: il, ic, nn
	open(unit = 11, & 
		file = trim(filename)//".obj", &
		form = "formatted", &
		access = "sequential", &
		action = "write", &
		status = "replace", &
		position = "rewind", &
		iostat = ios)
	IF ( ios /= 0 ) THEN
		print *,"\n PROBLEME A L'OUVERTURE\n"
		STOP
	END IF
	open(unit = 12, & 
		file = trim(filename)//"-geom.lxo", &
		form = "formatted", &
		access = "sequential", &
		action = "write", &
		status = "replace", &
		position = "rewind", &
		iostat = ios)
	IF ( ios /= 0 ) THEN
		print *,"\n PROBLEME A L'OUVERTURE\n"
		STOP
	END IF

	! informations autres que positions des points et facettes.
	write(unit=11,fmt='(A6,I6)') &
		'# y : ',ndim1
	write(unit=11,fmt='(A6,I6)') &
		'# x : ',ndim2
	write(unit=11,fmt='(A1)') ' '

	write(unit=12,fmt='(A)') "# LuxRender Geometry File"
	write(unit=12,fmt='(A)') "# Exported by Jason Champion"
	write(unit=12,fmt='(A)') "# ==========================="
	write(unit=12,fmt='(A)') " "
	write(unit=12,fmt='(A)') "AttributeBegin # ExportedSurface"
	write(unit=12,fmt='(A)') " "
	write(unit=12,fmt='(A)') '    NamedMaterial "Material.Lamb"'
	write(unit=12,fmt='(A)') " "
	write(unit=12,fmt='(A)') '    Shape "trianglemesh" "integer indices" ['

	! Coordonnees (x,y,z) des points.
	nn = 0
	DO il=1,ndim1,1
		DO ic=1,ndim2,1
			nn = nn + 1
			write(unit=11,fmt='(A2,F11.7,A1,F11.7,A1,F11.7)') &
				'v ',x(il,ic),' ',y(il,ic),' ',z(il,ic)
			n(il,ic) = nn
		END DO
	END DO

	IF (selectdiag .eq. 'NESO') THEN
			print *, "Case : NESO."
		r = 1
	ELSE IF (selectdiag .eq. 'NOSE') then
		print *, "Case : NOSE."
		r = 2
	ELSE IF (selectdiag .eq. 'rand') then
		print *, "Case : rand."
		call init_random_seed()
 		call random_number(r)
		where (r>=0.5)
			r = 2
		elsewhere
			r = 1
		end where
	ELSE
		print *, "ERROR : The lase argument of the ", &
			"subroutine 'MakeOljFile' should be either ", &
			" NOSE, NESO or rand."
		STOP
	END IF
			
	write(unit=11,fmt='(A1)') ' '
	write(unit=11,fmt='(A6)') 'g mesh'
	write(unit=11,fmt='(A1)') ' '
	DO il=1,ndim1-1,1
		DO ic=1,ndim2-1,1
			IF (r(il,ic)==1) THEN
				write(unit=11, fmt= &
					'(A2,I7,A1,I7,A1,I7)') & 
					'f ',n(il,ic),' ',n(il+1,ic+1), &
						' ',n(il+1,ic)
				write(unit=11, fmt= &
					'(A2,I7,A1,I7,A1,I7)') & 
					'f ',n(il,ic+1),' ',n(il+1,ic+1), &
					' ',n(il,ic)
				write(unit=12, fmt='(A,A,A,A,A)') & 
					trim(I2S(n(il,ic)-1)),' ',trim(I2S(n(il+1,ic+1)-1)),' ', &
					trim(I2S(n(il+1,ic)-1))
				write(unit=12, fmt='(A,A,A,A,A)') & 
					trim(I2S(n(il,ic+1)-1)),' ',trim(I2S(n(il+1,ic+1)-1)),' ', &
					trim(I2S(n(il,ic)-1))
			ELSE
				write(unit=11, fmt= &
					'(A2,I7,A1,I7,A1,I7)') & 
					'f ',n(il,ic+1),' ',n(il+1,ic), &
					' ',n(il,ic)
				write(unit=11, fmt= &
					'(A2,I7,A1,I7,A1,I7)') & 
					'f ',n(il+1,ic+1),' ',n(il+1,ic), &
					' ',n(il,ic+1)
				write(unit=12, fmt='(A,A,A,A,A)') & 
					trim(I2S(n(il,ic+1)-1)),' ',trim(I2S(n(il+1,ic)-1)),' ', &
					trim(I2S(n(il,ic)-1))
				write(unit=12, fmt='(A,A,A,A,A)') & 
					trim(I2S(n(il+1,ic+1)-1)),' ',trim(I2S(n(il+1,ic)-1)),' ', &
					trim(I2S(n(il,ic+1)-1))
			END IF
			END DO
	END DO
	write(unit=11,fmt='(A1)') ' '
	write(unit=11,fmt='(A1)') 'g'
	close(unit=11)

	write(unit=12,fmt='(A)') "    ]"
	write(unit=12,fmt='(A)') " "
	write(unit=12,fmt='(A)') '"point P" ['
	DO il=1,ndim1,1
		DO ic=1,ndim2,1
			write(unit=12,fmt='(F11.6,A1,F11.6,A1,F11.6)') &
				x(il,ic),' ',y(il,ic),' ',z(il,ic)
		END DO
	END DO
	write(unit=12,fmt='(A)') "    ]"
	write(unit=12,fmt='(A)') " "
	write(unit=12,fmt='(A)') "AttributeEnd"
	close(unit=12)

END SUBROUTINE

END MODULE
